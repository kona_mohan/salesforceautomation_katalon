<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_country</name>
   <tag></tag>
   <elementGuidId>12822a38-8a73-4efc-8dba-a5b227c3f0a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;full forcePageBlock forceRecordLayout&quot;]/div[1]//span[text()=&quot;Country&quot;]//following::a[@class=&quot;select&quot; and @aria-required=&quot;false&quot;][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
