<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_firstCaseInList</name>
   <tag></tag>
   <elementGuidId>0544a1f3-53a4-4b5a-b591-df0fc6af8bbd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[not(contains(@class,&quot;simpleEmptyState&quot;)) and contains(@class,&quot;test-listViewManager&quot;)]//*[@data-aura-class=&quot;uiVirtualDataTable&quot;]/tbody/tr[1]/th//a</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
