<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>casesHeader</name>
   <tag></tag>
   <elementGuidId>f4451757-4241-43a7-af10-756e617e501c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;column left-col&quot;]//*[@data-aura-class=&quot;forceRelatedListSingleContainer&quot;]//*[@title=&quot;Cases&quot; and text()=&quot;Cases&quot;]//parent::a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
