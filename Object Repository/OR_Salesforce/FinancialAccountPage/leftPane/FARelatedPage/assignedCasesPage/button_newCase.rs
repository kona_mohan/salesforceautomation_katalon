<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_newCase</name>
   <tag></tag>
   <elementGuidId>e0faf99b-5bd3-4500-b5ae-38c882f9f00c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@data-aura-class=&quot;forceListViewManager&quot;]//*[@data-aura-class=&quot;oneActionsRibbon forceActionsContainer&quot;]//a[@title=&quot;New&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
