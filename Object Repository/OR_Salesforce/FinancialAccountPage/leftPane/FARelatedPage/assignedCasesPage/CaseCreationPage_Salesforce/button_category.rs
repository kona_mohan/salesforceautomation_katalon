<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_category</name>
   <tag></tag>
   <elementGuidId>6e88e8ec-ea11-4227-85bb-d50a0a01566c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;test-id__record-layout-container riseTransitionEnabled&quot;]//*[@class=&quot;test-id__section slds-section  slds-is-open full forcePageBlockSection forcePageBlockSectionEdit&quot;][1]//span[text()=&quot;Category&quot;]//following::a[@class=&quot;select&quot;][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
