<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_FinancialAccount</name>
   <tag></tag>
   <elementGuidId>7443c4ab-e7a2-4280-8956-dcc6fb549ff4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;pageLevelErrors&quot;]//following::*[@class=&quot;test-id__section slds-section  slds-is-open full forcePageBlockSection forcePageBlockSectionEdit&quot;][1]//*[text()=&quot;Financial Account&quot;]//following::*[@class=&quot;pillText&quot;][1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
