<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_type</name>
   <tag></tag>
   <elementGuidId>1c93b67b-be51-41b3-9d3b-bfc186fbaa8f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;test-id__record-layout-container riseTransitionEnabled&quot;]//*[@class=&quot;test-id__section slds-section  slds-is-open full forcePageBlockSection forcePageBlockSectionEdit&quot;][2]//span[text()=&quot;Type&quot;]//following::a[@class=&quot;select&quot;][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
