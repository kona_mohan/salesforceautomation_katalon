<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_firstCard</name>
   <tag></tag>
   <elementGuidId>882f9962-5117-42dc-bdba-3d9c68e388bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;forceRelatedListDesktop&quot;]//table//tr[not(@class)][1]//th//a[contains(@title,&quot;Card&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
