<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_getHistoricalConsent</name>
   <tag></tag>
   <elementGuidId>d518fb19-c90c-4a9c-952e-962217648a5c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;pbBottomButtons&quot;]//input[@value=&quot;Get Historical Consent&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/iFrame</value>
   </webElementProperties>
</WebElementEntity>
