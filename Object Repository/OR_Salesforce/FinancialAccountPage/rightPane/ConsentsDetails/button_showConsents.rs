<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_showConsents</name>
   <tag></tag>
   <elementGuidId>6964f50d-ea8a-49d7-990e-279e2a92ca39</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@method=&quot;post&quot;]//input[contains(@id,&quot;consent&quot;) and @value]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/iFrame</value>
   </webElementProperties>
</WebElementEntity>
