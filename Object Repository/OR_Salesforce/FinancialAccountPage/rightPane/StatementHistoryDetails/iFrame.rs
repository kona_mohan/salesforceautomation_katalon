<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iFrame</name>
   <tag></tag>
   <elementGuidId>735e88bb-c4de-419a-b4c1-e0a9522a7f00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;column right-col&quot;]/*[@class=&quot;flexipageComponent&quot;]//li[@title=&quot;Statement History&quot;]//iframe</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
