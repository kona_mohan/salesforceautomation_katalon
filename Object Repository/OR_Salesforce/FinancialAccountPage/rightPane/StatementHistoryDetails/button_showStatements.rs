<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_showStatements</name>
   <tag></tag>
   <elementGuidId>0e984e03-722c-47a7-b488-9a9a961b520c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@method=&quot;post&quot;]//input[@class and @value]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/StatementHistoryDetails/iFrame</value>
   </webElementProperties>
</WebElementEntity>
