<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_statements</name>
   <tag></tag>
   <elementGuidId>282bfd9f-af39-4595-8558-8e5779af18ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form//*[@class='apexp']//table[@class]//tbody//tr</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/FAstatusAndBalanceDetails/iFrame</value>
   </webElementProperties>
</WebElementEntity>
