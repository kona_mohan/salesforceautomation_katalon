<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tab_firstStatement</name>
   <tag></tag>
   <elementGuidId>c468e942-e4b9-4c0b-a072-0c561ed29f77</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form//*[@class='apexp']//table[@class]//tbody//tr[3]/td[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/StatementHistoryDetails/iFrame</value>
   </webElementProperties>
</WebElementEntity>
