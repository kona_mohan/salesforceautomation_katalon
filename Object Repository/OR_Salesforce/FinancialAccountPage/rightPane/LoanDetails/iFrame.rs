<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iFrame</name>
   <tag></tag>
   <elementGuidId>20933a96-bf03-4c1c-868a-e2931dee2313</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;column right-col&quot;]/*[@class=&quot;flexipageComponent&quot;]//li[@title=&quot;Loan Details&quot;]//following::iframe</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
