<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tab_FAstatusAndBalance</name>
   <tag></tag>
   <elementGuidId>c5796c3e-3d51-4796-b486-314f989977e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;column right-col&quot;]/*[@class=&quot;flexipageComponent&quot;]//li[@title=&quot;Financial Account Status &amp; Balance&quot;]//button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
