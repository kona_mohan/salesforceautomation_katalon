<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iFrame</name>
   <tag></tag>
   <elementGuidId>5a217f61-624c-43ff-9b6f-755e988d75f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;column right-col&quot;]/*[@class=&quot;flexipageComponent&quot;]//li[@title=&quot;Financial Account Status &amp; Balance&quot;]//iframe</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
