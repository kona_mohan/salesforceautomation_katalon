<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_nextStatementDate</name>
   <tag></tag>
   <elementGuidId>14b3140b-dd9d-4771-b8c9-c0e65e0ef6ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@method=&quot;post&quot;]//tbody/tr[4]/td[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/FAstatusAndBalanceDetails/iFrame</value>
   </webElementProperties>
</WebElementEntity>
