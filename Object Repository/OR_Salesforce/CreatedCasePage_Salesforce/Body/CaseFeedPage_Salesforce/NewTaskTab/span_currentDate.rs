<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_currentDate</name>
   <tag></tag>
   <elementGuidId>33e9825c-6d4c-48be-8599-042a65fddb51</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;visible DESKTOP uiDatePicker--default uiDatePicker&quot;]//*[@aria-selected=&quot;true&quot;]/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
