<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropDown_ChangeOwnerName</name>
   <tag></tag>
   <elementGuidId>2464e99d-32a7-4787-967e-d6fb8764feb5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[contains(@title,&quot;Search &quot;) and @autocorrect]//following::div[@class=&quot;listContent&quot;]//ul/li[not(contains(@class,'invisible'))][1]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
