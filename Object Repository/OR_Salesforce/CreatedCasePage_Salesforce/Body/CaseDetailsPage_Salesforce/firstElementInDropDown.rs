<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>firstElementInDropDown</name>
   <tag></tag>
   <elementGuidId>56c3c2a2-96e4-4e82-ac70-65c515c88b33</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@title=&quot;Search Financial Accounts&quot;]//following::div[1]//li[contains(@class,'lookup__item  default uiAutocompleteOption')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
