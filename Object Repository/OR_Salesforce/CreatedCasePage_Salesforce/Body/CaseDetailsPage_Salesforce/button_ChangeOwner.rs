<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_ChangeOwner</name>
   <tag></tag>
   <elementGuidId>57b9bf98-6119-475e-8686-6c9b8740c4a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@data-aura-class=&quot;forceDetailPanelDesktop&quot;]//*[contains(@class,'showchangeOwnerLink')]//*[@class=&quot;ownerName&quot;]//following-sibling::button</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
