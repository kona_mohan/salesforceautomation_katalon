<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_EnableFinancialAccount</name>
   <tag></tag>
   <elementGuidId>0bed18e6-88ee-4edf-a8ba-028a43fef7a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@title = 'Edit Financial Account']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@data-aura-class=&quot;forceDetailPanelDesktop&quot;]//*[@class=&quot;full forcePageBlock forceRecordLayout&quot;]//*[@data-aura-class=&quot;forcePageBlockSection forcePageBlockSectionView&quot;][1]//*[@data-aura-class=&quot;forcePageBlockSectionRow&quot;][1]/div[2]//*[contains(@class,&quot;itemBody&quot;)]//button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Edit Financial Account</value>
   </webElementProperties>
</WebElementEntity>
