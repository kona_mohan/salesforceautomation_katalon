<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_closeOpenedSecondaryTabs</name>
   <tag></tag>
   <elementGuidId>0452a96f-d1f7-40d4-a32e-2dba62cb3b50</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;active hasFixedFooter oneWorkspace&quot;]//li[@class and contains(@class,'hasActions')][1]//div[contains(@class,'close')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
