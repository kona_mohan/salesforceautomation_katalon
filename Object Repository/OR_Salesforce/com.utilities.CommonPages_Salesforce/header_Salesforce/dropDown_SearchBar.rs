<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropDown_SearchBar</name>
   <tag></tag>
   <elementGuidId>0aaa8dcb-765d-4c46-b1e9-b9e34769f65b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@placeholder=&quot;Search Salesforce&quot;]//following::*[@class=&quot;lookup__list  visible&quot;]//li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
