<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_submit</name>
   <tag></tag>
   <elementGuidId>f9fcf7a3-7ff0-4c57-af73-8ed28bf5666c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;pbBottomButtons&quot;]//*[@value=&quot;Submit&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame</value>
   </webElementProperties>
</WebElementEntity>
