<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_extraCard_lastName</name>
   <tag></tag>
   <elementGuidId>498aa053-ba0e-4e04-8a90-24b4ce717922</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[text()=&quot;Last Name :&quot;]//following::input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame</value>
   </webElementProperties>
</WebElementEntity>
