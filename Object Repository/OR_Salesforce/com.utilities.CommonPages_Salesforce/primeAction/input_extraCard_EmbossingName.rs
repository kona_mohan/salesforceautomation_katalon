<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_extraCard_EmbossingName</name>
   <tag></tag>
   <elementGuidId>a26a9d0c-9335-4f52-bc81-37dd7fafe0a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[text()=&quot;Embossing Name :&quot;]//following::input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame</value>
   </webElementProperties>
</WebElementEntity>
