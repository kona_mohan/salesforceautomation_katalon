<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropDown_Nemkonto_TransactionType</name>
   <tag></tag>
   <elementGuidId>817de2ae-7f3c-4d0b-884f-7dff06102c96</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@id,'fundsTransfer') and contains(@name,'Type')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame</value>
   </webElementProperties>
</WebElementEntity>
