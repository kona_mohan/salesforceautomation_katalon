<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropDown_paymentHoliday_FAnumber</name>
   <tag></tag>
   <elementGuidId>607131f7-1886-4115-9d46-5b95cb148022</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[text()='Financial Account: ']//following::select[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame</value>
   </webElementProperties>
</WebElementEntity>
