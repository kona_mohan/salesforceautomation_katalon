<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>radioButton_createInstallment_loanPeriod</name>
   <tag></tag>
   <elementGuidId>8ea14ee3-cd27-4ce9-a411-c00f974db96d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@value='02'][count(. | //*[@ref_element = 'Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame']) = count(//*[@ref_element = 'Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='02']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@value='02']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame</value>
   </webElementProperties>
</WebElementEntity>
