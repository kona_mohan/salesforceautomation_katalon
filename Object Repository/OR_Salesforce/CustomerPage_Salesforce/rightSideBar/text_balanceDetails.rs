<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_balanceDetails</name>
   <tag></tag>
   <elementGuidId>98a6fc27-1fa8-4627-b5e4-c6e432ae74f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;financialAccTbl&quot;]//*[@class=&quot;pbBody&quot;]//table//tbody//tr[1]/td[7]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/CustomerPage_Salesforce/rightSideBar/frame_financialAccountDetails</value>
   </webElementProperties>
</WebElementEntity>
