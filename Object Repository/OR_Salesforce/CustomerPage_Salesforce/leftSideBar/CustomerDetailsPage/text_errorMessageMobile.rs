<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_errorMessageMobile</name>
   <tag></tag>
   <elementGuidId>945467d8-bf66-4064-ae3c-9162081a377c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body/span/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerDetailsPage/frame_mobileNotPresent</value>
   </webElementProperties>
</WebElementEntity>
