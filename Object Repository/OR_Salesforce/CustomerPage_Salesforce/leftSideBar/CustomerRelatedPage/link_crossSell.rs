<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_crossSell</name>
   <tag></tag>
   <elementGuidId>46197888-a157-41b4-a26c-7afa0ccba1f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;title&quot; and text()=&quot;Related&quot;]//following::section[2]//div[@class=&quot;container&quot;]//*[@title=&quot;Cross Sell Responses&quot;  and text()=&quot;Cross Sell Responses&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
