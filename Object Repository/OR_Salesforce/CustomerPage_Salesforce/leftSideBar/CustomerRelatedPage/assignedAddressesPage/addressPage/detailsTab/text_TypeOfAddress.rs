<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_TypeOfAddress</name>
   <tag></tag>
   <elementGuidId>005fe96a-9f42-432c-b45a-e3a8e36e28db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;flexipageBaseRecordHomeTemplateDesktop&quot;]//*[@data-component-id=&quot;force_detailPanel&quot;]//span[text()='Type of address']//following::span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
