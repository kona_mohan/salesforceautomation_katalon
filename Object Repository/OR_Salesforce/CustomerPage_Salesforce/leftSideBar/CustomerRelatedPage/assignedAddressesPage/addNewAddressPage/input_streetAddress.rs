<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_streetAddress</name>
   <tag></tag>
   <elementGuidId>2534559a-6857-47d1-bb58-205428ca5208</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/addressesPage/addNewAddressPage/iFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;bPageBlock brandSecondaryBrd bDetailBlock secondaryPalette&quot;]//*[@class=&quot;pbBody&quot;]//div[contains(@id,'addresspageblocksection')]//table[@class=&quot;detailList&quot;]//tr[1]//td[2]//div[@class=&quot;requiredInput&quot;]//textarea</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/iFrame</value>
   </webElementProperties>
</WebElementEntity>
