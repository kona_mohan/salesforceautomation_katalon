<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_city</name>
   <tag></tag>
   <elementGuidId>cd72b332-8afd-4ab6-8200-a3f73bec57e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot;pbSubsection&quot;]//*[@class=&quot;detailList&quot;]//tr[4]//td[1]//input[count(. | //*[@ref_element = 'Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/addressesPage/addNewAddressPage/iFrame']) = count(//*[@ref_element = 'Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/addressesPage/addNewAddressPage/iFrame'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;pbSubsection&quot;]//*[@class=&quot;detailList&quot;]//tr[4]//td[1]//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot;pbSubsection&quot;]//*[@class=&quot;detailList&quot;]//tr[4]//td[1]//input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/iFrame</value>
   </webElementProperties>
</WebElementEntity>
