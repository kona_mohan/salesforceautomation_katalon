<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_country</name>
   <tag></tag>
   <elementGuidId>9a5492a8-cc9d-436f-8a52-a01865e66ba2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@class=&quot;detailList&quot;]//tr[5]//td[1]/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/iFrame</value>
   </webElementProperties>
</WebElementEntity>
