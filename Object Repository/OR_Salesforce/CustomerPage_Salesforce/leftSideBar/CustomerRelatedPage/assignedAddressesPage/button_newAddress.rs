<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_newAddress</name>
   <tag></tag>
   <elementGuidId>6c13d6b4-bf95-449a-88ac-77947f81ff21</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'forceListViewManagerHeader')]/div[@class=&quot;slds-grid&quot;]//a[@href and @title=&quot;New&quot;] </value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
