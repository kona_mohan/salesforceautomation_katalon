<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_cards</name>
   <tag></tag>
   <elementGuidId>c49c4cf8-6da6-4471-b50d-84daa7a62949</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;title&quot; and text()=&quot;Related&quot;]//following::section[2]//div[@data-aura-class=&quot;forceRelatedListSingleContainer&quot;][9]//a[contains(@class,'baseCard__header-title')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
