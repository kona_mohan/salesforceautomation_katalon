<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_new</name>
   <tag></tag>
   <elementGuidId>e39de1e2-1663-40dd-b725-ece38b49ee56</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;forceRelatedListDesktop&quot;]//*[@data-aura-class=&quot;forceListViewManagerHeader&quot;]//li//a[@class=&quot;forceActionLink&quot; and @title=&quot;New&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
