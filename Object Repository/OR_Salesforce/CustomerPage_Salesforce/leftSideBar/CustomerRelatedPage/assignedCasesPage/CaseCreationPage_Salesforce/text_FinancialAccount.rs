<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_FinancialAccount</name>
   <tag></tag>
   <elementGuidId>5df7846f-f54c-42c6-b81c-528265e9157d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;pageLevelErrors&quot;]//following::*[@class=&quot;test-id__section slds-section  slds-is-open full forcePageBlockSection forcePageBlockSectionEdit&quot;][1]//*[text()=&quot;Financial Account&quot;]//following::*[@class=&quot;pillText&quot;][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
