<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_country</name>
   <tag></tag>
   <elementGuidId>f886fc49-76fa-4149-9d4d-540e170cfb9c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;full forcePageBlock forceRecordLayout&quot;]/div[1]//span[text()=&quot;Country&quot;]//following::a[@class=&quot;select&quot; and @aria-required=&quot;false&quot;][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
