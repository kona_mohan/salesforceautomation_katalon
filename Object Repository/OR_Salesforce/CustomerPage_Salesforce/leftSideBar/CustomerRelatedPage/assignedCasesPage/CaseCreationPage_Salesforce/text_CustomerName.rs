<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_CustomerName</name>
   <tag></tag>
   <elementGuidId>1705883c-4262-4607-ba82-779e66377a4e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;pageLevelErrors&quot;]//following::*[@class=&quot;test-id__section slds-section  slds-is-open full forcePageBlockSection forcePageBlockSectionEdit&quot;][1]//*[text()=&quot;Customer Name&quot;]//following::*[@class=&quot;pillText&quot;][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
