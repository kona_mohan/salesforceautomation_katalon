<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_newCase</name>
   <tag></tag>
   <elementGuidId>2f378782-4ed1-42c9-aa60-5de2d6ca9017</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@data-aura-class=&quot;forceListViewManager&quot;]//*[@data-aura-class=&quot;oneActionsRibbon forceActionsContainer&quot;]//a[@title=&quot;New&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
