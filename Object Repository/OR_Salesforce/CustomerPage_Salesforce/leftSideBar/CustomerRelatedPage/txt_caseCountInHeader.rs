<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_caseCountInHeader</name>
   <tag></tag>
   <elementGuidId>f4bd74d5-dd1e-4a3a-afef-d3e1d2386be1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;title&quot; and text()=&quot;Related&quot;]//following::section[2]//div[@class=&quot;container&quot;]//*[@title=&quot;Cases&quot; and text()=&quot;Cases&quot;]//following-sibling::span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
