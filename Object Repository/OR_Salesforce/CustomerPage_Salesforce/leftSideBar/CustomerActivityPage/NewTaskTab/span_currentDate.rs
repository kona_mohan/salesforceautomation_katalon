<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_currentDate</name>
   <tag></tag>
   <elementGuidId>b68bef07-d69a-4cf2-80d1-d8d95c868fbe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;visible DESKTOP uiDatePicker--default uiDatePicker&quot;]//*[@aria-selected=&quot;true&quot;]/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
