<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_edit</name>
   <tag></tag>
   <elementGuidId>f6df4179-d6d2-4596-96c4-f09fd6c870c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;column region-sidebar-right&quot;]//*[@data-aura-class=&quot;oneActionsRibbon forceActionsContainer&quot;]//a[@title=&quot;Edit&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
