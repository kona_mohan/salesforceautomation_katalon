<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_lastName</name>
   <tag></tag>
   <elementGuidId>5e6cb7fc-f5ab-450a-8135-23f114b1f8ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form//div[@class='pbSubsection']//*[text()='Last Name']//following::input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/iFrame</value>
   </webElementProperties>
</WebElementEntity>
