<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_mobileNumber</name>
   <tag></tag>
   <elementGuidId>bb8b9116-fdb3-4ac1-8095-0b0999f7458b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='pbSubsection']//*[text()='Mobile']//following::input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/iFrame</value>
   </webElementProperties>
</WebElementEntity>
