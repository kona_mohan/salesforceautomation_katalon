<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iFrame</name>
   <tag></tag>
   <elementGuidId>1e75b943-a1ba-485d-8e57-ed1bacc9187a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;oneAlohaPage&quot;]//div[contains(@class,'iframe slds-card iframe-parent')]/iframe</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
