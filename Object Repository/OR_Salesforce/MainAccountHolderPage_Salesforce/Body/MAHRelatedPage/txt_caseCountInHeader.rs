<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_caseCountInHeader</name>
   <tag></tag>
   <elementGuidId>c72e3318-23d9-4d89-b086-2ae282223c02</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;title&quot; and text()=&quot;Related&quot;]//following::section[2]//div[@class=&quot;container&quot;]//*[@title=&quot;Cases&quot; and text()=&quot;Cases&quot;]//following-sibling::span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
