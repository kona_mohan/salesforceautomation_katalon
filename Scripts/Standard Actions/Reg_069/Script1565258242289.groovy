import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'salesforcePages.loginPage_Salesforce.loginWithCredentials'(GlobalVariable.appilationURL, GlobalVariable.username, GlobalVariable.password)

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.searchAndSelectInSearchBox'(caseNumber)

CustomKeywords.'salesforcePages.CreatedCasePage_Salesforce_Center.clickAcceptButton'()

CustomKeywords.'salesforcePages.CreatedCasePage_Salesforce_Center.clickPrimeActionButton'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectPrimeAction'(primeAction)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectTransactionType'(transcType)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.setAmount'(transcAmount)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.clickSubmit'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.verifyErrorMessagePresent'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.verifySubmitButtonDisabled'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.closePrime'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllSecondaryTabs'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllTabs'()




