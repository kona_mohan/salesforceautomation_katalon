import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import salesforcePages.CustomerPage_Salesforce_Header

CustomKeywords.'salesforcePages.loginPage_Salesforce.loginWithCredentials'(GlobalVariable.appilationURL, GlobalVariable.username, GlobalVariable.password)

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(navigationMenuItem)

caseNumber=CustomKeywords.'salesforcePages.AllCasesPage_SalesForce.clickOnFirstOpenCaseInList'()

CustomKeywords.'salesforcePages.CreatedCasePage_Salesforce_Center.clickAcceptButton'()

CustomKeywords.'salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.verifyStatusAndChangeToInProgress'()

CustomKeywords.'salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.changeFinancialAccount'(FinancialAccountName)

CustomKeywords.'salesforcePages.CreatedCasePage_Salesforce_Center.clickPrimeActionButton'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectPrimeAction'(primeActionType)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.verifyCustomerNameAutoPopulated'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.verifyAllFieldsMandatory'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectReplaceCardNumber'(replaceCardNumber)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.clickSubmit'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.closePrime'()

CustomKeywords.'com.utilities.CreatedCasePage_SalesForce_Body.clickOnRelatedTab'()

CustomKeywords.'salesforcePages.CreatedCasePage_SalesForce_Body_PrimeActionLog.verifyPrimeLog'(primeActionType)

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_Salesforce.clickonRelatedTab'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_centerPane.clickOnPrimeAction'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectPrimeAction'(primeActionType_PCB)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.verifyCustomerNameAutoPopulated'()

//CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.verifyAllFieldsMandatory'()





