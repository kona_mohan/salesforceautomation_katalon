import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'salesforcePages.loginPage_Salesforce.loginWithCredentials'(GlobalVariable.appilationURL, GlobalVariable.username, GlobalVariable.password)

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(navigationMenuItem)

CustomKeywords.'salesforcePages.AllCasesPage_SalesForce.clickOnFirstCaseInList'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_centerPane.clickOnPrimeAction'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectPrimeAction'(primeActionType_PH)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.verifyCustomerNameAutoPopulated'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.verifyAllFieldsMandatory'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectFinancialAccountNumber'(financialAccountNumber)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.clickSubmit'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.closePrime'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_Salesforce.clickonRelatedTab'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_primeActionLog.verifyPrimeTypeInLog'(primeActionType_PH)

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_centerPane.clickOnPrimeAction'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectPrimeAction'(primeActionType_CA)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.verifyCustomerNameAutoPopulated'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.verifyAllFieldsMandatory'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectCloseAccountReason'(closedAcccountReason)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.clickSubmit'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.closePrime'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_Salesforce.clickonRelatedTab'()

//CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_primeActionLog.verifyPrimeTypeInLog'(primeActionType_CA)

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_centerPane.clickOnPrimeAction'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectPrimeAction'(primeActionType_CI)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.chooseLoanPeriodInMonths'(loanPeriod)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.clickSubmit'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.closePrime'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_Salesforce.clickonRelatedTab'()

//CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_primeActionLog.verifyPrimeTypeInLog'(primeActionType_CI)

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_Salesforce.clickonRelatedTab'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_Salesforce.clickOnCardsHeader'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCardsPage_Salesforce.clickOnFirstCard'()

CustomKeywords.'salesforcePages.CardsPage_Salesforce_Header.clickPrimeAction'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.selectPrimeAction'(primeActionType_RC)

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.clickSubmit'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.primeAction.closePrime'()












