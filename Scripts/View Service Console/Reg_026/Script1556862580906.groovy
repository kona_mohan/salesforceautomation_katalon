import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.testng.Assert

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'salesforcePages.loginPage_Salesforce.loginWithCredentials'(GlobalVariable.appilationURL, GlobalVariable.username, GlobalVariable.password)

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(navigationMenuItem)

CustomKeywords.'salesforcePages.AllCasesPage_SalesForce.clickOnFirstCaseInList'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar.clickRelatedTab'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.clickOnAddressesHeader'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage.clickOnNewAddressButton'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.verifyTypeOfAddressIsTextBox'(addressType)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.verifyStreetAddressMaxLength'(streetAddressMaxLength)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.verifyCityMaxLength'(cityMaxLength)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.verifyZipMaxLength'(zipMaxLength)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.verifyCountryAutoFilled'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllSecondaryTabs'()

//CustomKeywords.'salesforcePages.CustomerPage_Salesforce_RightSideBar.verifyBalanceInfo'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar.clickRelatedTab'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.clickOnMAHHeader'()

CustomKeywords.'salesforcePages.AllCasesPage_SalesForce.clickOnFirstCaseInList'()

CustomKeywords.'salesforcePages.MainAccountHolderPage_Salesforce_Body_MAHDetailsPage.verifyFieldsInDetailsPage'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllSecondaryTabs'()





