import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.Robot
import java.awt.event.KeyEvent

import org.openqa.selenium.Keys

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import salesforcePages.CustomerPage_Salesforce_LeftSideBar

CustomKeywords.'salesforcePages.loginPage_Salesforce.loginWithCredentials'(GlobalVariable.appilationURL, GlobalVariable.username, GlobalVariable.password)

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(navigationMenuItem)

CustomKeywords.'salesforcePages.AllCasesPage_SalesForce.clickOnFirstCaseInList'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_Salesforce.clickonRelatedTab'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_Salesforce.clickOnCardsHeader'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCardsPage_Salesforce.verifyCardsAllocated'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCardsPage_Salesforce.clickOnFirstCard'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllTabs'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(anotherNavigationMenuItem)

CustomKeywords.'salesforcePages.AllCasesPage_SalesForce.clickOnFirstCaseInList'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar.clickRelatedTab'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.clickOnCardsHeader'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCardsPage.verifyCardsAllocated'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCardsPage.clickOnFirstCard'()

//CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllTabs'()


