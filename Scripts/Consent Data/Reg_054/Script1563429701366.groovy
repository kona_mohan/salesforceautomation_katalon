import java.text.DateFormat
import java.text.SimpleDateFormat

import internal.GlobalVariable as GlobalVariable

CustomKeywords.'salesforcePages.loginPage_Salesforce.loginWithCredentials'(GlobalVariable.appilationURL, GlobalVariable.username, GlobalVariable.password)

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(navigationMenuItem)

CustomKeywords.'salesforcePages.AllCasesPage_SalesForce.clickOnFirstCaseInList'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_rightPane_Salesforce.clickOnConsentsTab'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.clickShowConsent'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.selectConsentChannels'(consentChannel)

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.clickUpdateConsent'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.verifySuccessMessageInGreen'()

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.clickGetHistoricalConsent'()
DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")
Date date = new Date();

CustomKeywords.'salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.fetchConsentHistory'(fromDate,dateFormat.format(date))




