import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.Robot
import java.awt.event.KeyEvent

import org.openqa.selenium.By
import org.openqa.selenium.Dimension
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebElement
import org.testng.Assert

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.main.CustomKeywordDelegatingMetaClass
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'salesforcePages.loginPage_Salesforce.loginWithCredentials'(GlobalVariable.appilationURL, GlobalVariable.username, GlobalVariable.password)

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(c_navigationItem)

CustomKeywords.'salesforcePages.AllCasesPage_SalesForce.clickOnFirstOpenCaseInList'()

CustomKeywords.'salesforcePages.CreatedCasePage_SalesForce_Body.clickOnFeedTab'()

CustomKeywords.'salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_Salesforce.clickOnNewTask'()

CustomKeywords.'salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_NewTaskTab.setSubject'(taskSubject_C)

CustomKeywords.'salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_NewTaskTab.SetDate'()

CustomKeywords.'salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_NewTaskTab.clickSave'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllTabs'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(Cu_navigationItem)

CustomKeywords.'salesforcePages.AllCasesPage_SalesForce.clickOnFirstCaseInList'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar.clickActivityTab'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage.clickNewTaskTab'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage_NewTaskPage.setSubject'(taskSubject_L)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage_NewTaskPage.setDate'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage_NewTaskPage.clickSave'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllTabs'()