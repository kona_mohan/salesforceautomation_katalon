import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.sql.ResultSet

import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.helper.commonFunctions.ConnectPrime


CustomKeywords.'salesforcePages.loginPage_Salesforce.loginWithCredentials'(GlobalVariable.appilationURL, GlobalVariable.username, GlobalVariable.password)

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(navigationMenuItem)

String serno =CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.searchAndSelectInSearchBox'("select serno from caddresses where location!='"+typeOfAddress+"' AND ROWNUM = 1")

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar.clickRelatedTab'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.clickOnAddressesHeader'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage.clickOnNewAddressButton'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.enterTypeOfAddress'(typeOfAddress)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.enterStreetAddress'(streetAddress)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.enterCity'(city)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.enterZipCode'(zipCode)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.clickOnSave'()

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllSecondaryTabs'()

CustomKeywords.'com.helper.commonFunctions.ConnectPrime.executeStatementAndVerify'("select location from caddresses where serno='"+serno+"'",typeOfAddress)
