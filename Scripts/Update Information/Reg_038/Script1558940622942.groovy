import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable



CustomKeywords.'salesforcePages.loginPage_Salesforce.loginWithCredentials'(GlobalVariable.appilationURL, GlobalVariable.username, GlobalVariable.password)

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(navigationMenuItem)

CustomKeywords.'salesforcePages.AllCasesPage_SalesForce.clickOnCasewithSSNNumber'()

String serno=CustomKeywords.'salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerDetailsPage.getSerNo'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_Header.clickOnEditButton'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.verifyEmailAndMobileNumberFieldsNotMandatory'()

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.enterFirstName'(firstName)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.enterLastName'(lastName)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.enterEmail'(email)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.enterMobileNumber'(mobile)

CustomKeywords.'salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.clickOnSave'() 

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllSecondaryTabs'()

CustomKeywords.'com.helper.commonFunctions.ConnectPrime.executeStatementAndVerify'("SELECT firstname FROM people WHERE serno='"+serno+"'", firstName)

CustomKeywords.'com.helper.commonFunctions.ConnectPrime.executeStatementAndVerify'("SELECT lastname FROM people WHERE serno='"+serno+"'", lastName)



