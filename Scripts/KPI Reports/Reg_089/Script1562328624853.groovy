import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'salesforcePages.loginPage_Salesforce.loginWithCredentials'(GlobalVariable.appilationURL, GlobalVariable.username, GlobalVariable.password)

CustomKeywords.'com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu'(navigationMenuItem)

CustomKeywords.'salesforcePages.ReportsPage_SideFolderBar.clickOnAllReports'()

CustomKeywords.'salesforcePages.ReportsPage_Header.searchReport'(reportNameCCM)

CustomKeywords.'salesforcePages.ReportsPage_Body.verifyReportPresent'(reportNameCCM)

CustomKeywords.'salesforcePages.ReportsPage_Header.searchReport'(reportNameCCY)

CustomKeywords.'salesforcePages.ReportsPage_Body.verifyReportPresent'(reportNameCCY)

CustomKeywords.'salesforcePages.ReportsPage_Header.searchReport'(reportNameCCMTD)

CustomKeywords.'salesforcePages.ReportsPage_Body.verifyReportPresent'(reportNameCCMTD)

CustomKeywords.'salesforcePages.ReportsPage_Header.searchReport'(reportNameCCYTD)

CustomKeywords.'salesforcePages.ReportsPage_Body.verifyReportPresent'(reportNameCCYTD)

CustomKeywords.'salesforcePages.ReportsPage_Header.searchReport'(reportNameAHTMTD)

CustomKeywords.'salesforcePages.ReportsPage_Body.verifyReportPresent'(reportNameAHTMTD)





