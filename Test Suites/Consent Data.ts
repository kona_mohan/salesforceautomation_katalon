<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Consent Data</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>441723f3-f779-43bf-9551-9a4fd1a27890</testSuiteGuid>
   <testCaseLink>
      <guid>0ad59c15-e1c6-419c-9164-bff1586339b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Consent Data/Reg_054</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e0e16c90-9b8d-4cd0-af36-35df2922c379</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ConsentData</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>e0e16c90-9b8d-4cd0-af36-35df2922c379</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>navigationMenuItem</value>
         <variableId>e83ad09d-357f-4d51-8a33-fd5c8ee50b39</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e0e16c90-9b8d-4cd0-af36-35df2922c379</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>consentChannel</value>
         <variableId>20fb70af-909d-48cc-b820-478d97e51988</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>88603429-bfc1-4a58-be84-a8e521b5341f</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
