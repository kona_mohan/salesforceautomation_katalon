<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Inbound Communication</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2915c349-ed4f-4fc9-8f31-7c72e021c126</testSuiteGuid>
   <testCaseLink>
      <guid>a321bb97-160c-48fc-8c06-581f359b4eb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Inbound Communication/Reg_073</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1f118fb4-9fd1-45a5-bf1c-d6664362c517</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/InboundCommunicationData</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>1f118fb4-9fd1-45a5-bf1c-d6664362c517</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>navigationMenuItem</value>
         <variableId>fba9f429-3db6-439d-9551-77ea573c47e3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1f118fb4-9fd1-45a5-bf1c-d6664362c517</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ownerTypeQ</value>
         <variableId>8b3e2762-eaee-4e5b-9d36-529bb2c511bb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1f118fb4-9fd1-45a5-bf1c-d6664362c517</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>queueName</value>
         <variableId>ca6a4d0e-f897-4342-946f-4d1505046ddb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1f118fb4-9fd1-45a5-bf1c-d6664362c517</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ownerTypeP</value>
         <variableId>b4a76988-1ea1-440a-89a9-1ac6894b553d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1f118fb4-9fd1-45a5-bf1c-d6664362c517</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ownerName</value>
         <variableId>26a8bb5b-a88f-4e19-8f23-b80108fe47a3</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
