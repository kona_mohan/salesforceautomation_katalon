<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Outbound Communication</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b3ea1d67-55df-405e-80a4-0a26ca63c57d</testSuiteGuid>
   <testCaseLink>
      <guid>58063192-985c-4cca-a626-af9db494c083</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/OutBound Communicatioon/Reg_083</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>40b104d1-d2c8-4ef0-87d1-25e91dbe1f69</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/OutboundCommunicationsData</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>40b104d1-d2c8-4ef0-87d1-25e91dbe1f69</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>navigationMenuItem</value>
         <variableId>1a576349-6cc4-4f67-8d03-495ebd9a94d4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>40b104d1-d2c8-4ef0-87d1-25e91dbe1f69</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>emailTemplate</value>
         <variableId>6fe2faae-0579-4460-8904-fb0e766a2f8d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
