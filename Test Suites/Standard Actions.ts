<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Standard Actions</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>64e0a9e9-ea63-426b-b594-186f9d602e44</testSuiteGuid>
   <testCaseLink>
      <guid>47792c7a-8f4c-4268-b48f-b8d4c3647344</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Standard Actions/Reg_056</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f4082d3a-19bb-46f1-82a0-8d20826e1d97</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/StandardActionsData</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>f4082d3a-19bb-46f1-82a0-8d20826e1d97</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>navigationMenuItem_C</value>
         <variableId>5d45564d-325c-4ae4-a8f9-2c09364fed35</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f4082d3a-19bb-46f1-82a0-8d20826e1d97</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>caseInProgressStatus</value>
         <variableId>5364158d-4790-476c-8a56-1c929c5d1fda</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f4082d3a-19bb-46f1-82a0-8d20826e1d97</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>financialAccount</value>
         <variableId>a0d2d0d8-9cb5-4e30-ab1b-ce1c47c95849</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f4082d3a-19bb-46f1-82a0-8d20826e1d97</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>primeActionType_RC</value>
         <variableId>cd74b421-f8be-4908-833c-9624a8b17964</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f4082d3a-19bb-46f1-82a0-8d20826e1d97</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>replaceCardNumber</value>
         <variableId>e412d474-d286-4c9b-8929-af7640273c06</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f4082d3a-19bb-46f1-82a0-8d20826e1d97</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>primeActionType_PCB</value>
         <variableId>19e19109-0684-4f53-9288-724247ce2b17</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5a41480f-f27b-4fc4-8d2c-313b95876710</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2c331c14-8ea3-494e-bd46-866216b1ff7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Standard Actions/Reg_057</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1a0d008c-0911-44a4-b5c9-3c76d67f8331</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/StandardActionsData</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>1a0d008c-0911-44a4-b5c9-3c76d67f8331</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>navigationMenuItem_FA</value>
         <variableId>507a0012-d06f-4e93-9b40-65f123baef65</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1a0d008c-0911-44a4-b5c9-3c76d67f8331</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>primeActionType_PH</value>
         <variableId>10c7d568-2c27-48c9-bf4e-7086f1211061</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1a0d008c-0911-44a4-b5c9-3c76d67f8331</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>FinancialAccountNumber_PH</value>
         <variableId>0031ce08-0df6-4e90-b31a-7b99b446f5f3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1a0d008c-0911-44a4-b5c9-3c76d67f8331</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>primeActionType_CA</value>
         <variableId>5846180f-2b6a-4875-aab3-49f4dbe49dea</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1a0d008c-0911-44a4-b5c9-3c76d67f8331</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>closedAcccountReason</value>
         <variableId>251e5494-6368-4e24-91a1-7e2042408690</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1a0d008c-0911-44a4-b5c9-3c76d67f8331</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>primeActionType_CI</value>
         <variableId>ea593d67-40d8-4c0f-bea8-4695a1744ad4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1a0d008c-0911-44a4-b5c9-3c76d67f8331</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>loanPeriod_CI</value>
         <variableId>47a0a804-2ab3-4f81-8437-674b3e982fc6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1a0d008c-0911-44a4-b5c9-3c76d67f8331</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>primeActionType_RC</value>
         <variableId>cebd256f-71d4-4723-a918-595dcb412588</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b3216445-6d79-4c14-802c-5b0a69b6fef6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Standard Actions/Reg_062</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>38ae88d4-4266-48c3-8529-3b142952b664</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>400a2c6c-922a-4f04-96fc-50f62d583628</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2001685e-5709-4a6c-bf8f-b0031f19eab0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fb783496-c70b-4248-a347-7570bcc522ce</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7d8807d2-37b3-4a86-94a9-c1b1e834f9aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Standard Actions/Reg_065</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>92f8ca2c-74ae-4903-b7e1-cb5181607711</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>84a2102c-d46b-4558-85fc-dd3c1bb70721</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b0d7badc-0362-4e85-bde4-1f1aabe616fb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fa2303e3-5fe1-4fee-ae7c-9f932a687dc4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7eb686cd-60c0-4c27-8a63-b745c3494a45</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1e9d29ba-867c-4b99-ba17-d31d5268bfb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Standard Actions/Reg_068</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2338caef-591a-4a55-bb25-8fe7bdf38cad</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>839e921f-0801-4fc0-8f7d-ce13d0c28c93</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8a2b93e2-24b1-44a7-a125-b9f270cb2bc8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b15e753c-2777-4faf-9b7f-b176c33346ce</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d2197291-2ea4-4eec-8b81-be694f21badd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Standard Actions/Reg_069</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>24b6c130-86b3-46b5-a7c6-1d152b006273</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>32ac4185-afa8-4220-8474-7b7b09b611c9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>107fff58-aa1a-492a-9e8d-0293fc49f517</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1d774b1d-2326-475c-8e62-c3ff91252d28</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
