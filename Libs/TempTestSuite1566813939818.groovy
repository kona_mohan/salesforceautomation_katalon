import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/View Service Console')

suiteProperties.put('name', 'View Service Console')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\MohanKo\\salesforceautomation_katalon\\Reports\\View Service Console\\20190826_120535\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/View Service Console', suiteProperties, [new TestCaseBinding('Test Cases/View Service Console/Reg_021', 'Test Cases/View Service Console/Reg_021',  [ 'anotherNavigationMenuItem' : 'Customers' , 'navigationMenuItem' : 'Financial Accounts' ,  ]), new TestCaseBinding('Test Cases/View Service Console/Reg_024', 'Test Cases/View Service Console/Reg_024',  [ 'navigationMenuItem' : 'Customers' ,  ]), new TestCaseBinding('Test Cases/View Service Console/Reg_026', 'Test Cases/View Service Console/Reg_026',  [ 'cityMaxLength' : '20' , 'streetAddressMaxLength' : '256' , 'addressType' : 'input' , 'zipMaxLength' : '10' , 'navigationMenuItem' : 'Customers' ,  ]), new TestCaseBinding('Test Cases/View Service Console/Reg_027', 'Test Cases/View Service Console/Reg_027',  [ 'navigationMenuItem' : 'Financial Accounts' ,  ]), new TestCaseBinding('Test Cases/View Service Console/Reg_028', 'Test Cases/View Service Console/Reg_028',  [ 'financialAccountNumber' : 'FA-3014771' , 'navigationMenuItem' : 'Financial Accounts' ,  ])])
