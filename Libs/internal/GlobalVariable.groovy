package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object appilationURL
     
    /**
     * <p></p>
     */
    public static Object username
     
    /**
     * <p></p>
     */
    public static Object password
     
    /**
     * <p></p>
     */
    public static Object ownerName
     
    /**
     * <p></p>
     */
    public static Object databaseURL
     
    /**
     * <p></p>
     */
    public static Object databaseUser
     
    /**
     * <p></p>
     */
    public static Object databasePassword
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            appilationURL = selectedVariables['appilationURL']
            username = selectedVariables['username']
            password = selectedVariables['password']
            ownerName = selectedVariables['ownerName']
            databaseURL = selectedVariables['databaseURL']
            databaseUser = selectedVariables['databaseUser']
            databasePassword = selectedVariables['databasePassword']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
