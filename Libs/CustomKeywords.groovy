
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import java.sql.ResultSet


def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar.clickRelatedTab"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar()).clickRelatedTab()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar.clickActivityTab"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar()).clickActivityTab()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab_EmailTemplatePopUp.selectTemplate"(
    	String templateType	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab_EmailTemplatePopUp()).selectTemplate(
        	templateType)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_assignedCrossSellResponse_CreateSellResponse.selectCrosSellSuccess"(
    	String yesOrNo	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_assignedCrossSellResponse_CreateSellResponse()).selectCrosSellSuccess(
        	yesOrNo)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_assignedCrossSellResponse_CreateSellResponse.selectCountry"(
    	String country	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_assignedCrossSellResponse_CreateSellResponse()).selectCountry(
        	country)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_assignedCrossSellResponse_CreateSellResponse.selectServiceSold"(
    	String serviceSold	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_assignedCrossSellResponse_CreateSellResponse()).selectServiceSold(
        	serviceSold)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_assignedCrossSellResponse_CreateSellResponse.clickSave"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_assignedCrossSellResponse_CreateSellResponse()).clickSave()
}

def static "com.utilities.CommonPages_Salesforce.header_Salesforce.searchInSearchBox"(
    	String input	) {
    (new com.utilities.CommonPages_Salesforce.header_Salesforce()).searchInSearchBox(
        	input)
}

def static "com.utilities.CommonPages_Salesforce.header_Salesforce.searchAndSelectInSearchBox"(
    	String valueToBeSearched	) {
    (new com.utilities.CommonPages_Salesforce.header_Salesforce()).searchAndSelectInSearchBox(
        	valueToBeSearched)
}

def static "com.utilities.CommonPages_Salesforce.header_Salesforce.verifyfilterOptionPresent"() {
    (new com.utilities.CommonPages_Salesforce.header_Salesforce()).verifyfilterOptionPresent()
}

def static "com.utilities.CommonPages_Salesforce.header_Salesforce.selectItemFromNavigationMenu"(
    	String menuItem	) {
    (new com.utilities.CommonPages_Salesforce.header_Salesforce()).selectItemFromNavigationMenu(
        	menuItem)
}

def static "com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllTabs"() {
    (new com.utilities.CommonPages_Salesforce.header_Salesforce()).closeAllTabs()
}

def static "com.utilities.CommonPages_Salesforce.header_Salesforce.closeAllSecondaryTabs"() {
    (new com.utilities.CommonPages_Salesforce.header_Salesforce()).closeAllSecondaryTabs()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_assignedCrossSellResponse.clickNewButton"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_assignedCrossSellResponse()).clickNewButton()
}

def static "salesforcePages.CardsPage_Salesforce_Header.clickPrimeAction"() {
    (new salesforcePages.CardsPage_Salesforce_Header()).clickPrimeAction()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.verifyCaseCountallocatedToCustomer"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage()).verifyCaseCountallocatedToCustomer()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.clickOnCasesHeader"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage()).clickOnCasesHeader()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.clickOnCardsHeader"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage()).clickOnCardsHeader()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.clickOnAddressesHeader"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage()).clickOnAddressesHeader()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.clickOnCrossSellHeader"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage()).clickOnCrossSellHeader()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.clickOnMAHHeader"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage()).clickOnMAHHeader()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage.clickOnAlertHeader"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage()).clickOnAlertHeader()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AlertsPage.clickOnNewButton"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AlertsPage()).clickOnNewButton()
}

def static "salesforcePages.CaseCreationPage_Salesforce.clickSaveCase"() {
    (new salesforcePages.CaseCreationPage_Salesforce()).clickSaveCase()
}

def static "salesforcePages.CaseCreationPage_Salesforce.selectcaseOrigin"(
    	String caseOrigin	) {
    (new salesforcePages.CaseCreationPage_Salesforce()).selectcaseOrigin(
        	caseOrigin)
}

def static "salesforcePages.CaseCreationPage_Salesforce.selectCategory"(
    	String category	) {
    (new salesforcePages.CaseCreationPage_Salesforce()).selectCategory(
        	category)
}

def static "salesforcePages.CaseCreationPage_Salesforce.selectType"(
    	String Type	) {
    (new salesforcePages.CaseCreationPage_Salesforce()).selectType(
        	Type)
}

def static "salesforcePages.CaseCreationPage_Salesforce.selectCountry"(
    	String country	) {
    (new salesforcePages.CaseCreationPage_Salesforce()).selectCountry(
        	country)
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_Salesforce.clickonRelatedTab"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_Salesforce()).clickonRelatedTab()
}

def static "salesforcePages.CreatedCasePage_SalesForce_Body.clickOnFeedTab"() {
    (new salesforcePages.CreatedCasePage_SalesForce_Body()).clickOnFeedTab()
}

def static "salesforcePages.CreatedCasePage_SalesForce_Body.clickOnRelatedTab"() {
    (new salesforcePages.CreatedCasePage_SalesForce_Body()).clickOnRelatedTab()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseRelatedPage_Nemkonto.verifyTrasactionPresent"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseRelatedPage_Nemkonto()).verifyTrasactionPresent()
}

def static "salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.enterFirstName"(
    	String fName	) {
    (new salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage()).enterFirstName(
        	fName)
}

def static "salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.enterLastName"(
    	String lName	) {
    (new salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage()).enterLastName(
        	lName)
}

def static "salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.enterEmail"(
    	String email	) {
    (new salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage()).enterEmail(
        	email)
}

def static "salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.enterMobileNumber"(
    	String mobileNo	) {
    (new salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage()).enterMobileNumber(
        	mobileNo)
}

def static "salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.verifyEmailAndMobileNumberFieldsNotMandatory"() {
    (new salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage()).verifyEmailAndMobileNumberFieldsNotMandatory()
}

def static "salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage.clickOnSave"() {
    (new salesforcePages.CustomerPage_Salesforce_Header_EditCustomerDetailsPage()).clickOnSave()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCasePage_CaseCreationPage_Salesforce.verifyCustomerName"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCasePage_CaseCreationPage_Salesforce()).verifyCustomerName()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerDetailsPage.getMobileNumber"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerDetailsPage()).getMobileNumber()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerDetailsPage.getSerNo"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerDetailsPage()).getSerNo()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerDetailsPage.verifyErrorMessageIfMobileNumberNotPresent"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerDetailsPage()).verifyErrorMessageIfMobileNumberNotPresent()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerDetailsPage.verifyAlertImagePresent"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerDetailsPage()).verifyAlertImagePresent()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_ChangeOwnerPopUp_Salesforce.EnterOwnerName"(
    	String ownerName	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_ChangeOwnerPopUp_Salesforce()).EnterOwnerName(
        	ownerName)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_ChangeOwnerPopUp_Salesforce.EnterQueue"(
    	String queue	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_ChangeOwnerPopUp_Salesforce()).EnterQueue(
        	queue)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_ChangeOwnerPopUp_Salesforce.changeOwnerType"(
    	String ownerType	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_ChangeOwnerPopUp_Salesforce()).changeOwnerType(
        	ownerType)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseRelatedPage_CaseHistory.verifyChangedOwnerName"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseRelatedPage_CaseHistory()).verifyChangedOwnerName()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.verifyTypeOfAddressIsTextBox"(
    	String tagName	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage()).verifyTypeOfAddressIsTextBox(
        	tagName)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.enterTypeOfAddress"(
    	String typeOfAddress	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage()).enterTypeOfAddress(
        	typeOfAddress)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.verifyStreetAddressMaxLength"(
    	String maxLength	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage()).verifyStreetAddressMaxLength(
        	maxLength)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.enterStreetAddress"(
    	String streetAddress	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage()).enterStreetAddress(
        	streetAddress)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.verifyCityMaxLength"(
    	String maxLength	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage()).verifyCityMaxLength(
        	maxLength)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.enterCity"(
    	String city	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage()).enterCity(
        	city)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.verifyZipMaxLength"(
    	String maxLength	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage()).verifyZipMaxLength(
        	maxLength)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.enterZipCode"(
    	String zipcode	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage()).enterZipCode(
        	zipcode)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.verifyCountryAutoFilled"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage()).verifyCountryAutoFilled()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage.clickOnSave"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage()).clickOnSave()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage.clickNewTaskTab"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage()).clickNewTaskTab()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCardsPage_Salesforce.verifyCardsAllocated"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCardsPage_Salesforce()).verifyCardsAllocated()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCardsPage_Salesforce.clickOnFirstCard"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCardsPage_Salesforce()).clickOnFirstCard()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_primeActionLog.verifyPrimeTypeInLog"(
    	String primeType	) {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_primeActionLog()).verifyPrimeTypeInLog(
        	primeType)
}

def static "salesforcePages.AllSearchPage_Salesforce.verifySearchedElementPresent"() {
    (new salesforcePages.AllSearchPage_Salesforce()).verifySearchedElementPresent()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_FAStatusAndBalanceDetails.clickOnGetFAstatusAndBalanceDetails"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_FAStatusAndBalanceDetails()).clickOnGetFAstatusAndBalanceDetails()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_FAStatusAndBalanceDetails.verifyNextStatementDatePresent"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_FAStatusAndBalanceDetails()).verifyNextStatementDatePresent()
}

def static "com.helper.commonFunctions.ConnectPrime.executeStatementAndVerify"(
    	String sqlQuery	
     , 	String valueToBeVerified	) {
    (new com.helper.commonFunctions.ConnectPrime()).executeStatementAndVerify(
        	sqlQuery
         , 	valueToBeVerified)
}

def static "com.helper.commonFunctions.ConnectPrime.executeStatement"(
    	String sqlQuery	) {
    (new com.helper.commonFunctions.ConnectPrime()).executeStatement(
        	sqlQuery)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseRelatedPage_Attachments.uploadRequiredAttachment"(
    	Object docPath	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseRelatedPage_Attachments()).uploadRequiredAttachment(
        	docPath)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseRelatedPage_Attachments.clickOnViewAttachments"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseRelatedPage_Attachments()).clickOnViewAttachments()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseRelatedPage_Attachments.clickOnDeleteAttachment"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseRelatedPage_Attachments()).clickOnDeleteAttachment()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_NotesAndAttachments.uploadRequiredAttachment"(
    	String DocPath	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_NotesAndAttachments()).uploadRequiredAttachment(
        	DocPath)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_NotesAndAttachments.clickOnViewAttachments"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_NotesAndAttachments()).clickOnViewAttachments()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_NotesAndAttachments.clickOnDeleteAttachment"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_NotesAndAttachments()).clickOnDeleteAttachment()
}

def static "salesforcePages.MainAccountHolderPage_Salesforce_Body_MAHDetailsPage.verifyFieldsInDetailsPage"() {
    (new salesforcePages.MainAccountHolderPage_Salesforce_Body_MAHDetailsPage()).verifyFieldsInDetailsPage()
}

def static "salesforcePages.ReportsPage_Header.searchReport"(
    	String reportName	) {
    (new salesforcePages.ReportsPage_Header()).searchReport(
        	reportName)
}

def static "salesforcePages.CreatedCasePage_SalesForce_Body_CaseRelatedPage.clickOnNemkontoHeader"() {
    (new salesforcePages.CreatedCasePage_SalesForce_Body_CaseRelatedPage()).clickOnNemkontoHeader()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage.clickOnNewAddressButton"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage()).clickOnNewAddressButton()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_centerPane.clickOnPrimeAction"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_centerPane()).clickOnPrimeAction()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AlertsPage_CreateAlertPage.selectCurrentDate"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AlertsPage_CreateAlertPage()).selectCurrentDate()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AlertsPage_CreateAlertPage.clickOnSave"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AlertsPage_CreateAlertPage()).clickOnSave()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Center.clickAcceptButton"(
    	String ownerName	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Center()).clickAcceptButton(
        	ownerName)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Center.clickPrimeActionButton"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Center()).clickPrimeActionButton()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_Salesforce.clickOnCasesHeader"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_Salesforce()).clickOnCasesHeader()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_Salesforce.clickOnCardsHeader"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_Salesforce()).clickOnCardsHeader()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_Salesforce.clickOnCrossSellResponse"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_Salesforce()).clickOnCrossSellResponse()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_PostTab_Salesforce.sendPost"(
    	String postText	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_PostTab_Salesforce()).sendPost(
        	postText)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_PostTab_Salesforce.verifyPostInAllUpdates"(
    	String postText	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_PostTab_Salesforce()).verifyPostInAllUpdates(
        	postText)
}

def static "com.utilities.CommonPages_Salesforce.primeAction.selectPrimeAction"(
    	String primeAction	) {
    (new com.utilities.CommonPages_Salesforce.primeAction()).selectPrimeAction(
        	primeAction)
}

def static "com.utilities.CommonPages_Salesforce.primeAction.clickSubmit"() {
    (new com.utilities.CommonPages_Salesforce.primeAction()).clickSubmit()
}

def static "com.utilities.CommonPages_Salesforce.primeAction.verifySubmitButtonDisabled"() {
    (new com.utilities.CommonPages_Salesforce.primeAction()).verifySubmitButtonDisabled()
}

def static "com.utilities.CommonPages_Salesforce.primeAction.selectReplaceCardNumber"(
    	String cardNumber	) {
    (new com.utilities.CommonPages_Salesforce.primeAction()).selectReplaceCardNumber(
        	cardNumber)
}

def static "com.utilities.CommonPages_Salesforce.primeAction.selectFinancialAccountNumber"(
    	String faNumber	) {
    (new com.utilities.CommonPages_Salesforce.primeAction()).selectFinancialAccountNumber(
        	faNumber)
}

def static "com.utilities.CommonPages_Salesforce.primeAction.selectCloseAccountReason"(
    	String reason	) {
    (new com.utilities.CommonPages_Salesforce.primeAction()).selectCloseAccountReason(
        	reason)
}

def static "com.utilities.CommonPages_Salesforce.primeAction.verifyAllFieldsMandatory"() {
    (new com.utilities.CommonPages_Salesforce.primeAction()).verifyAllFieldsMandatory()
}

def static "com.utilities.CommonPages_Salesforce.primeAction.chooseLoanPeriodInMonths"(
    	String months	) {
    (new com.utilities.CommonPages_Salesforce.primeAction()).chooseLoanPeriodInMonths(
        	months)
}

def static "com.utilities.CommonPages_Salesforce.primeAction.verifyCustomerNameAutoPopulated"() {
    (new com.utilities.CommonPages_Salesforce.primeAction()).verifyCustomerNameAutoPopulated()
}

def static "com.utilities.CommonPages_Salesforce.primeAction.downloadTransactionInPDF"() {
    (new com.utilities.CommonPages_Salesforce.primeAction()).downloadTransactionInPDF()
}

def static "com.utilities.CommonPages_Salesforce.primeAction.setStartDateAndEndDate"(
    	String StartDate	
     , 	String EndDate	) {
    (new com.utilities.CommonPages_Salesforce.primeAction()).setStartDateAndEndDate(
        	StartDate
         , 	EndDate)
}

def static "com.utilities.CommonPages_Salesforce.primeAction.selectExistingCard"() {
    (new com.utilities.CommonPages_Salesforce.primeAction()).selectExistingCard()
}

def static "com.utilities.CommonPages_Salesforce.primeAction.setSSNNumber"(
    	String SSN	) {
    (new com.utilities.CommonPages_Salesforce.primeAction()).setSSNNumber(
        	SSN)
}

def static "com.utilities.CommonPages_Salesforce.primeAction.verifyFirstNameAndLastNameAndEmbossingNameAutoPopulated"() {
    (new com.utilities.CommonPages_Salesforce.primeAction()).verifyFirstNameAndLastNameAndEmbossingNameAutoPopulated()
}

def static "com.utilities.CommonPages_Salesforce.primeAction.selectTransactionType"(
    	String transcType	) {
    (new com.utilities.CommonPages_Salesforce.primeAction()).selectTransactionType(
        	transcType)
}

def static "com.utilities.CommonPages_Salesforce.primeAction.setAmount"(
    	String transcAmount	) {
    (new com.utilities.CommonPages_Salesforce.primeAction()).setAmount(
        	transcAmount)
}

def static "com.utilities.CommonPages_Salesforce.primeAction.verifyErrorMessagePresent"() {
    (new com.utilities.CommonPages_Salesforce.primeAction()).verifyErrorMessagePresent()
}

def static "com.utilities.CommonPages_Salesforce.primeAction.closePrime"() {
    (new com.utilities.CommonPages_Salesforce.primeAction()).closePrime()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_Salesforce.clickOnPost"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_Salesforce()).clickOnPost()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_Salesforce.clickOnNewTask"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_Salesforce()).clickOnNewTask()
}

def static "salesforcePages.CustomerPage_Salesforce_RightSideBar.verifyBalanceInfo"() {
    (new salesforcePages.CustomerPage_Salesforce_RightSideBar()).verifyBalanceInfo()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddressPage_DetailsTab.verifyAddressFieldInPrime"(
    	ResultSet result	
     , 	String valueToBeVerified	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddressPage_DetailsTab()).verifyAddressFieldInPrime(
        	result
         , 	valueToBeVerified)
}

def static "salesforcePages.MainAccountHolderPage_Salesforce_Body_MAHRelatedPage.verifyCaseCountallocatedToMAH"() {
    (new salesforcePages.MainAccountHolderPage_Salesforce_Body_MAHRelatedPage()).verifyCaseCountallocatedToMAH()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCasesPage_CaseCreationPage_Salesforce.verifyCustomerName"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCasesPage_CaseCreationPage_Salesforce()).verifyCustomerName()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCardsPage.verifyCardsAllocated"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCardsPage()).verifyCardsAllocated()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCardsPage.clickOnFirstCard"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCardsPage()).clickOnFirstCard()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCasesPage_Salesforce.clickNewButton"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCasesPage_Salesforce()).clickNewButton()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_loanDetails.clickOnGetLoanDetailsButton"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_loanDetails()).clickOnGetLoanDetailsButton()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_statementHistoryDetail.clickOnShowStatements"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_statementHistoryDetail()).clickOnShowStatements()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_statementHistoryDetail.scrollRightToLastElement"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_statementHistoryDetail()).scrollRightToLastElement()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_statementHistoryDetail.verifyCycleDelayedField"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_statementHistoryDetail()).verifyCycleDelayedField()
}

def static "salesforcePages.CreatedCasePage_SalesForce_Body_PrimeActionLog.verifyPrimeLog"(
    	String PrimeActionLog	) {
    (new salesforcePages.CreatedCasePage_SalesForce_Body_PrimeActionLog()).verifyPrimeLog(
        	PrimeActionLog)
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse_createCrossSellResponse.selectCrosSellSuccess"(
    	String yesOrNo	) {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse_createCrossSellResponse()).selectCrosSellSuccess(
        	yesOrNo)
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse_createCrossSellResponse.selectCountry"(
    	String country	) {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse_createCrossSellResponse()).selectCountry(
        	country)
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse_createCrossSellResponse.selectServiceSold"(
    	String serviceSold	) {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse_createCrossSellResponse()).selectServiceSold(
        	serviceSold)
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse_createCrossSellResponse.clickSave"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse_createCrossSellResponse()).clickSave()
}

def static "salesforcePages.ReportsPage_SideFolderBar.clickOnAllReports"() {
    (new salesforcePages.ReportsPage_SideFolderBar()).clickOnAllReports()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse.clickNewButton"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse()).clickNewButton()
}

def static "salesforcePages.MainAccountHolderPage_Salesforce_Body.clickRelatedTab"() {
    (new salesforcePages.MainAccountHolderPage_Salesforce_Body()).clickRelatedTab()
}

def static "salesforcePages.ReportsPage_Body.clickOnFirstReport"() {
    (new salesforcePages.ReportsPage_Body()).clickOnFirstReport()
}

def static "salesforcePages.ReportsPage_Body.clickOnFirstReportDropDownButton"() {
    (new salesforcePages.ReportsPage_Body()).clickOnFirstReportDropDownButton()
}

def static "salesforcePages.ReportsPage_Body.clickOnExportInDropDown"() {
    (new salesforcePages.ReportsPage_Body()).clickOnExportInDropDown()
}

def static "salesforcePages.ReportsPage_Body.clickOnExportButtonInPopUp"() {
    (new salesforcePages.ReportsPage_Body()).clickOnExportButtonInPopUp()
}

def static "salesforcePages.ReportsPage_Body.verifyExcelFileCreatedInLocalDriver"(
    	String path	
     , 	String folderName	) {
    (new salesforcePages.ReportsPage_Body()).verifyExcelFileCreatedInLocalDriver(
        	path
         , 	folderName)
}

def static "salesforcePages.ReportsPage_Body.verifyReportPresent"(
    	String reportName	) {
    (new salesforcePages.ReportsPage_Body()).verifyReportPresent(
        	reportName)
}

def static "salesforcePages.CustomerPage_Salesforce_Header.clickOnEditButton"() {
    (new salesforcePages.CustomerPage_Salesforce_Header()).clickOnEditButton()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab.clickOnAddEmail"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab()).clickOnAddEmail()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab.enterToRecipient"(
    	String recipient	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab()).enterToRecipient(
        	recipient)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab.enterSubject"(
    	String subject	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab()).enterSubject(
        	subject)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab.enterTextInBody"(
    	String matter	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab()).enterTextInBody(
        	matter)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab.clickSendMail"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab()).clickSendMail()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab.clickOnInsertTemplate"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab()).clickOnInsertTemplate()
}

def static "salesforcePages.loginPage_Salesforce.loginWithCredentials"(
    	String applicationURL	
     , 	String Uname	
     , 	String pwd	) {
    (new salesforcePages.loginPage_Salesforce()).loginWithCredentials(
        	applicationURL
         , 	Uname
         , 	pwd)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage_NewTaskPage.setSubject"(
    	String subject	) {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage_NewTaskPage()).setSubject(
        	subject)
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage_NewTaskPage.setDate"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage_NewTaskPage()).setDate()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage_NewTaskPage.clickSave"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage_NewTaskPage()).clickSave()
}

def static "salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCasePage_Salesforce.clickNewButton"() {
    (new salesforcePages.CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCasePage_Salesforce()).clickNewButton()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Header.getCaseNumber"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Header()).getCaseNumber()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Header.verifyStatus"(
    	String status	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Header()).verifyStatus(
        	status)
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_Salesforce.clickOnLoanDetailsTab"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_Salesforce()).clickOnLoanDetailsTab()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_Salesforce.clickOnFAstatusAndBalanceTab"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_Salesforce()).clickOnFAstatusAndBalanceTab()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_Salesforce.clickOnStatementHistoryTab"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_Salesforce()).clickOnStatementHistoryTab()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_Salesforce.clickOnConsentsTab"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_Salesforce()).clickOnConsentsTab()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_NewTaskTab.setSubject"(
    	String subject	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_NewTaskTab()).setSubject(
        	subject)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_NewTaskTab.SetDate"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_NewTaskTab()).SetDate()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_NewTaskTab.clickSave"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseFeedPage_NewTaskTab()).clickSave()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.verifyTypeInCreatedCase"(
    	String type	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).verifyTypeInCreatedCase(
        	type)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.verifyCaseOriginInCreatedCase"(
    	String caseOrigin	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).verifyCaseOriginInCreatedCase(
        	caseOrigin)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.verifyCountryInCreatedCase"(
    	String country	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).verifyCountryInCreatedCase(
        	country)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.verifyStatusInCreatedCase"(
    	String Status	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).verifyStatusInCreatedCase(
        	Status)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.verifyStatusAndChangeToInProgress"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).verifyStatusAndChangeToInProgress()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.verifyPriorityInCreatedCase"(
    	String Priority	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).verifyPriorityInCreatedCase(
        	Priority)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.clickToEnableButtonType"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).clickToEnableButtonType()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.selectType"(
    	String type	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).selectType(
        	type)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.clickSave"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).clickSave()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.verifyCaseHandlingTimeIsNotNull"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).verifyCaseHandlingTimeIsNotNull()
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.clickOnChangeOwner"(
    	String CaseOwnerName	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).clickOnChangeOwner(
        	CaseOwnerName)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.verifyOwnerName"(
    	String ownerName	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).verifyOwnerName(
        	ownerName)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.changeStatus"(
    	String newStatus	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).changeStatus(
        	newStatus)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.changeFinancialAccount"(
    	String FAname	) {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).changeFinancialAccount(
        	FAname)
}

def static "salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.verifyCustomerNamePresent"() {
    (new salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce()).verifyCustomerNamePresent()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.clickShowConsent"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails()).clickShowConsent()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.verifySuccessMessageInGreen"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails()).verifySuccessMessageInGreen()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.clickUpdateConsent"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails()).clickUpdateConsent()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.clickGetHistoricalConsent"() {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails()).clickGetHistoricalConsent()
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.selectConsentChannels"(
    	String consent	) {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails()).selectConsentChannels(
        	consent)
}

def static "salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails.fetchConsentHistory"(
    	String fromDate	
     , 	String toDate	) {
    (new salesforcePages.FinancialAccountPage_Salesforce_rightPane_ConsentsDetails()).fetchConsentHistory(
        	fromDate
         , 	toDate)
}

def static "salesforcePages.AllCasesPage_SalesForce.clickNewButton"() {
    (new salesforcePages.AllCasesPage_SalesForce()).clickNewButton()
}

def static "salesforcePages.AllCasesPage_SalesForce.selectCaseNumberFromTable"(
    	String caseNumber	) {
    (new salesforcePages.AllCasesPage_SalesForce()).selectCaseNumberFromTable(
        	caseNumber)
}

def static "salesforcePages.AllCasesPage_SalesForce.clickOnFirstOpenCaseInList"() {
    (new salesforcePages.AllCasesPage_SalesForce()).clickOnFirstOpenCaseInList()
}

def static "salesforcePages.AllCasesPage_SalesForce.clickOnFirstCaseInList"() {
    (new salesforcePages.AllCasesPage_SalesForce()).clickOnFirstCaseInList()
}

def static "salesforcePages.AllCasesPage_SalesForce.clickOnCasewithOwnerName"(
    	String ownername	) {
    (new salesforcePages.AllCasesPage_SalesForce()).clickOnCasewithOwnerName(
        	ownername)
}

def static "salesforcePages.AllCasesPage_SalesForce.clickOnCasewithSSNNumber"() {
    (new salesforcePages.AllCasesPage_SalesForce()).clickOnCasewithSSNNumber()
}

def static "salesforcePages.AllCasesPage_SalesForce.clickOnCaseWithStatusNewOrProgress"() {
    (new salesforcePages.AllCasesPage_SalesForce()).clickOnCaseWithStatusNewOrProgress()
}

def static "salesforcePages.AllCasesPage_SalesForce.selectSubCategory"(
    	String subCategory	) {
    (new salesforcePages.AllCasesPage_SalesForce()).selectSubCategory(
        	subCategory)
}

def static "salesforcePages.AllCasesPage_SalesForce.verifyCaseInDKFraudAndChargeback"(
    	String caseNumber	) {
    (new salesforcePages.AllCasesPage_SalesForce()).verifyCaseInDKFraudAndChargeback(
        	caseNumber)
}

def static "salesforcePages.AllCasesPage_SalesForce.clickChangeOwnerInDKFraudAndChargeback"(
    	String caseNumber	) {
    (new salesforcePages.AllCasesPage_SalesForce()).clickChangeOwnerInDKFraudAndChargeback(
        	caseNumber)
}
