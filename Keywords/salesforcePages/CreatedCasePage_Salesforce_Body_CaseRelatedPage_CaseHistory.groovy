package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


import com.helper.commonFunctions.JavaScriptFunctions
import salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_ChangeOwnerPopUp_Salesforce

public class CreatedCasePage_Salesforce_Body_CaseRelatedPage_CaseHistory {

	public static String currentOwner
	@Keyword
	public void verifyChangedOwnerName() {
		WebUI.delay(5)
		JavaScriptFunctions.scrollDownByPixel("1000")
		WebUI.delay(5)
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/CaseHistory/text_newOwnerName'), 60)
		WebUI.delay(5)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/CaseHistory/text_newOwnerName'), 60)
		currentOwner=WebUI.getText(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/CaseHistory/text_newOwnerName'))
		Assert.assertEquals(currentOwner ,salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.getOwnerName)
	}
}
