package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.testng.Assert


import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class FinancialAccountPage_Salesforce_rightPane_statementHistoryDetail {

	@Keyword
	public void clickOnShowStatements() {
		WebUI.delay(5)
//		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/StatementHistoryDetails/button_showStatements'),40)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/StatementHistoryDetails/button_showStatements'),40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/StatementHistoryDetails/button_showStatements'))
	}

	@Keyword
	public void scrollRightToLastElement() {
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/StatementHistoryDetails/tab_firstStatement'))
		WebUI.sendKeys(null, Keys.chord(Keys.ARROW_RIGHT))
		WebUI.sendKeys(null, Keys.chord(Keys.ARROW_RIGHT))
	}

	@Keyword
	public void verifyCycleDelayedField() {
		List<WebElement> statementList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/StatementHistoryDetails/list_statements'), 10)
		for(int i=0;i<=statementList.size()-1;i++) {
			println i
			String fieldValidation= statementList.get(i).findElement(By.xpath(".//tr[9]")).getText()
			if(fieldValidation.equalsIgnoreCase("1")) {
				String attribute = statementList.get(i).findElement(By.xpath(".//tr[9]")).getAttribute("style")
				Assert.assertEquals(attribute,'background-color: red')
			}
		}
	}
}
