package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI



public class CaseCreationPage_Salesforce {

	public void selectFromDropDown(TestObject obj, String valuetoBeSelected) {
		List<WebElement> allElements = WebUiCommonHelper.findWebElements(obj, 20)
		for(int i=0;i<=allElements.size()-1;i++) {
			String dropDownValue=allElements.get(i).findElement(By.xpath(".//a")).getText()
			if(valuetoBeSelected.equalsIgnoreCase(dropDownValue)) {
				allElements.get(i).click()
				break
			}
		}
	}

	@Keyword
	public void clickSaveCase() {
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CaseCreationPage_Salesforce/button_Save'), 50)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CaseCreationPage_Salesforce/button_Save'))
	}

	@Keyword
	public void selectcaseOrigin(String caseOrigin) {
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/CaseCreationPage_Salesforce/button_caseOrigin'), 50)
		WebUI.click(findTestObject('OR_Salesforce/CaseCreationPage_Salesforce/button_caseOrigin'))
		WebUI.delay(2)
		selectFromDropDown(findTestObject('OR_Salesforce/CaseCreationPage_Salesforce/commonDropDown'), caseOrigin)
	}

	@Keyword
	public void selectCategory(String category) {
		WebUI.scrollToElement(findTestObject('OR_Salesforce/CaseCreationPage_Salesforce/button_caseOrigin'), 50)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CaseCreationPage_Salesforce/button_category'))
		WebUI.delay(2)
		selectFromDropDown(findTestObject('OR_Salesforce/CaseCreationPage_Salesforce/commonDropDown'), category)
	}

	@Keyword
	public void selectType(String Type) {
		WebUI.scrollToElement(findTestObject('OR_Salesforce/CaseCreationPage_Salesforce/button_category'), 50)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CaseCreationPage_Salesforce/button_type'), 50)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CaseCreationPage_Salesforce/button_type'))
		WebUI.delay(2)
		selectFromDropDown(findTestObject('OR_Salesforce/CaseCreationPage_Salesforce/commonDropDown'), Type)
		WebUI.delay(1)
	}

	@Keyword
	public void selectCountry(String country) {
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CaseCreationPage_Salesforce/button_country'), 50)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CaseCreationPage_Salesforce/button_country'))
		WebUI.delay(2)
		selectFromDropDown(findTestObject('OR_Salesforce/CaseCreationPage_Salesforce/commonDropDown'), country)
	}
}
