package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI



import com.helper.commonFunctions.JavaScriptFunctions

import com.helper.commonFunctions.ActionClassFunctions
public class CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce {

	public void selectFromDropDown(TestObject obj, String valuetoBeSelected) {
		List<WebElement> allElements = WebUiCommonHelper.findWebElements(obj, 20)
		for(int i=0;i<=allElements.size()-1;i++) {
			String dropDownValue=allElements.get(i).findElement(By.xpath(".//a")).getText()
			if(valuetoBeSelected.equalsIgnoreCase(dropDownValue)) {
				allElements.get(i).click()
				break
			}
		}
	}

	public static String actualType
	@Keyword
	public void verifyTypeInCreatedCase(String type) {
		WebUI.delay(5)
		try{
			WebDriver driver = DriverFactory.getWebDriver()
			WebElement typeText = WebUiCommonHelper.findWebElement(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_Type'), 60)
			println WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_Type'), 50)
			if(WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_Type'), 50)) {
//				WebUI.delay(5)
				actualType=typeText.getAttribute("innerText")
				println actualType
				Assert.assertEquals(actualType,type)
			}
			else
			{
				WebUI.scrollToElement(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_Type'), 30)
				actualType=typeText.getAttribute("innerText")
				println actualType
				Assert.assertEquals(actualType,type)
			}
		}catch(Exception e) {
//			WebUI.refresh()
//			WebUI.delay(5)
//			JavaScriptFunctions.scrollDownByPixel('1000')
//			WebUI.delay(5)
//			println "incatch"
//			if(WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_Type'), 50)) {
//				actualType=WebUI.getText(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_Type'))
//				println actualType
//				Assert.assertEquals(type,actualType)
//			}
		}
	}

	public static String actualCaseOrigin
	@Keyword
	public void verifyCaseOriginInCreatedCase(String caseOrigin) {
		WebUI.delay(5)
		if(WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_CaseOrigin'), 50)) {
			actualCaseOrigin=WebUI.getAttribute(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_CaseOrigin'),"innerText")
			println actualCaseOrigin
			Assert.assertEquals(caseOrigin,actualCaseOrigin)
		}
	}

	public static String actualCountry
	@Keyword
	public void verifyCountryInCreatedCase(String country) {
		if(WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_Country'), 50)) {
			actualCountry=WebUI.getAttribute(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_Country'),"innerText")
			Assert.assertEquals(country,actualCountry)
		}
	}

	public static String actualStatus
	@Keyword
	public void verifyStatusInCreatedCase(String Status) {
		WebUI.delay(3)
			if(WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_status'), 50)) {
				actualStatus=WebUI.getAttribute(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_status'),"innerText")
				Assert.assertEquals(Status,actualStatus)
			}
	}

	@Keyword
	public void verifyStatusAndChangeToInProgress() {
		WebUI.delay(2)
		try{
			WebUI.refresh()
			println "in if"
//			WebUI.delay(2)
			WebUI.delay(2)
			JavaScriptFunctions.scrollDownByPixel("500")
			if(WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_status'), 50)) {
				actualStatus=WebUI.getText(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_status'))
				println actualStatus
			}
				else{
					WebUI.refresh()
					println "in else"
					WebUI.delay(5)
					JavaScriptFunctions.scrollDownByPixel("1000")
					JavaScriptFunctions.scrollToElement(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_status'))
					if(WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_status'), 50)) {
						actualStatus=WebUI.getText(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_status'))
						println actualStatus
					}
				}
		}catch(Exception e){}
		
		if(actualStatus.equalsIgnoreCase("New") || actualStatus.equalsIgnoreCase("Re open")) {
			JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_status'))
			WebUI.delay(10)
			WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/dropDown_status'), 60)
			JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/dropDown_status'))
			selectFromDropDown(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/commonDropDown'), "In Progress")
			WebUI.waitForElementClickable(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'), 60)
			WebUI.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'))
		}
		else if(actualStatus.equalsIgnoreCase("Closed")) {
			JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_status'))
			WebUI.delay(10)
			WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/dropDown_status'), 60)
			JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/dropDown_status'))
			selectFromDropDown(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/commonDropDown'), "Re Open")
			WebUI.waitForElementClickable(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'), 60)
			JavaScriptFunctions.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'))
			WebUI.delay(10)
			JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_status'))
			WebUI.delay(10)
			WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/dropDown_status'), 60)
			JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/dropDown_status'))
			selectFromDropDown(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/commonDropDown'), "In Progress")
			WebUI.waitForElementClickable(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'), 60)
			WebUI.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'))
		}
		else {
		}
	}

	public static String actualPriority
	@Keyword
	public void verifyPriorityInCreatedCase(String Priority) {
		WebUI.delay(2)
		if(WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_priority'), 50)) {
			actualPriority=WebUI.getAttribute(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_priority'),"innerText")
			Assert.assertEquals(Priority,actualPriority)
		}
	}

	@Keyword
	public void clickToEnableButtonType() {
		WebUI.delay(5)
		JavaScriptFunctions.scrollDownByPixel('1000')
		WebUI.delay(2)
		WebUI.waitForElementClickable(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_EnableType'), 60)
		JavaScriptFunctions.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_EnableType'))
	}

	@Keyword
	public void selectType(String type) {
		//		JavaScriptFunctions.scrollDownByPixel('1000')
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/dropDown_Type'), 60)
		WebUI.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/dropDown_Type'))
		selectFromDropDown(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/commonDropDown'), type)

	}

	@Keyword
	public void clickSave() {
		WebUI.waitForElementClickable(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'), 60)
		WebUI.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'))
		//		if(getStatus.equalsIgnoreCase("closed") && nextStatus!="Reopen") {
		//			WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Error'), 60)
		//		}
	}

	@Keyword
	public void verifyCaseHandlingTimeIsNotNull() {
		WebUI.refresh()
		WebUI.delay(2)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_caseHandlingTime'), 10)
		println WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_caseHandlingTime'),"innerText")
		Assert.assertNotNull(WebUI.getText(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_caseHandlingTime')))
	}



	public static String getOwnerName;
	@Keyword
	public void clickOnChangeOwner(String CaseOwnerName) {
		WebUI.delay(5)
		println CaseOwnerName
		println "fetched"
		getOwnerName=getCaseOwnerName()
		println getOwnerName
		if(getOwnerName.equalsIgnoreCase(CaseOwnerName))
		{
			KeywordUtil.markErrorAndStop(CaseOwnerName+" already owns this case")
		}
		else
		{
			WebUI.delay(2)
			WebUI.waitForElementVisible(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_ChangeOwner'), 60)
			WebUI.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_ChangeOwner'))
		}
		
	}

	public static String getCaseOwnerName() {
		WebUI.refresh()
		WebUI.delay(2)
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_ownerName'), 40)
		println WebUI.getAttribute(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_ownerName'),"innerText")
		return WebUI.getAttribute(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_ownerName'),"innerText")
	}

	@Keyword
	public void verifyOwnerName(String ownerName) {
		WebUI.delay(5)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_ownerName'), 60)
		String OwnerNameInDOM=WebUiCommonHelper.findWebElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_ownerName'), 30).getAttribute("innerText")
	
		Assert.assertEquals(OwnerNameInDOM, ownerName)
	}

	public static String getStatus,nextStatus;

	@Keyword
	public void changeStatus(String newStatus) {
		WebUI.delay(3)
		if(WebUI.verifyElementNotPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Header/alert_changeOwnerMessage'), 10))
		{
		nextStatus=newStatus
		try{
		if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_status'), 20))
		{
			
		}
		else{
			JavaScriptFunctions.scrollDownByPixel('1000')
			WebUI.delay(2)
		}}catch(Exception e){}
		getStatus=WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_status'),"innerText")
		println getStatus
		try{
			WebUI.refresh()
			WebUI.delay(5)
		if(WebUI.verifyElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_status')))
		{
			WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_status'))
			WebUI.delay(2)
		}
		else
		{
			WebUI.refresh()
			WebUI.delay(2)
			WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_status'))
		}}catch(Exception e){}
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/dropDown_status'), 60)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/dropDown_status'))
		selectFromDropDown(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/commonDropDown'), newStatus)
		}
	}

	@Keyword
	public void changeFinancialAccount(String FAname) {
		WebUI.refresh()
		WebUI.waitForPageLoad(5)
		try{
			println "in try"
			println WebUI.verifyElementNotPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_financialAccount'),20)
			if(WebUI.verifyElementNotPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_financialAccount'),20)) {
				WebUI.delay(2)
				if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_EnableFinancialAccount'), 10)) {
					println "click button"
//					WebUI.mouseOver(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_EnableFinancialAccount'))
					WebUI.delay(1)
					JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_EnableFinancialAccount'))
					WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/input_FinancialAccount'), 60)
					WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/input_FinancialAccount'), FAname)
					WebUI.delay(2)
					WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/firstElementInDropDown'))
					WebUI.waitForElementClickable(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'), 60)
					JavaScriptFunctions.scrollDownByPixel("50")
					WebUI.delay(2)
					WebUI.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'))
				}
				else {
					WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Center/button_accept'), 60)
					println "scrolled up"
					WebUI.delay(5)
					WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_EnableFinancialAccount'))
					WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/input_FinancialAccount'), 60)
					WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/input_FinancialAccount'), FAname)
					WebUI.delay(5)
					WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/firstElementInDropDown'))
					WebUI.waitForElementClickable(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'), 60)
					WebUI.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/button_Save'))
				}
			}
		}catch(Exception e) {
		}
	}

	@Keyword
	public void verifyCustomerNamePresent() {
		WebUI.delay(5)
		Assert.assertTrue(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_CustomerName'), 60))
	}
}



