package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

import com.helper.commonFunctions.JavaScriptFunctions

public class CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_NotesAndAttachments {
	
	@Keyword 
	public void uploadRequiredAttachment(String DocPath)
	{
		
		try{
			WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_alerts'), 20)
			WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/button_uploadAttachment'), 40)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/button_uploadAttachment'), DocPath)
		WebUI.delay(5)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/img_uploadSuccess'), 10)
		WebUI.delay(6)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/button_DoneUploading'))
		WebUI.delay(2)
		}catch(Exception e)
		{
			WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_addresses'), 20)
			WebUI.delay(3)
			WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/button_uploadAttachment'), 40)
			WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/button_uploadAttachment'), DocPath)
			WebUI.delay(2)
			WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/img_uploadSuccess'), 10)
			WebUI.delay(8)
			WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/button_DoneUploading'))
			WebUI.delay(3)
		}
		WebUI.delay(10)
	}
	
	@Keyword
	public void clickOnViewAttachments()
	{
		WebUI.delay(3)
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/link_ViewAllAttachments'), 40)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/link_ViewAllAttachments'), 40)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/link_ViewAllAttachments'), 20)
//		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/link_ViewAllAttachments'))
		JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/link_ViewAllAttachments'))
	}
	
	@Keyword
	public void clickOnDeleteAttachment()
	{
		WebUI.delay(3)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/button_dropdownAttachment'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/button_dropdownAttachment'))
		WebUI.delay(1)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/dropdown_delete'), 30)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/dropdown_delete'))
		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/notesAndAttachments/button_confirmDeleteAttachment'))
	}
}
