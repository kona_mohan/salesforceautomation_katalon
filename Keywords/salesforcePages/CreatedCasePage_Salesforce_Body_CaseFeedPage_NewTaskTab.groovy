package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


public class CreatedCasePage_Salesforce_Body_CaseFeedPage_NewTaskTab {
	
	@Keyword
	public void setSubject(String subject)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/NewTaskTab/input_Subject'), 20)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/NewTaskTab/input_Subject'), subject)	
//		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/NewTaskTab/input_Subject'), Keys.chord(Keys.TAB))
	}
	
	@Keyword
	public void SetDate()
	{
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/NewTaskTab/button_DueDate'), 30)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/NewTaskTab/button_DueDate'), 60)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/NewTaskTab/button_DueDate'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/NewTaskTab/span_currentDate'), 30)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/NewTaskTab/span_currentDate'))
	}
	
	@Keyword
	public void clickSave()
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/NewTaskTab/button_Save'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/NewTaskTab/button_Save'))
	}
}
