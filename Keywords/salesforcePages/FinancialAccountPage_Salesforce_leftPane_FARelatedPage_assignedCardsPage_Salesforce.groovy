package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement
import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCardsPage_Salesforce {

	@Keyword
	public verifyCardsAllocated() {
		WebUI.delay(5)
		WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCardsPage/list_cards'), 60)
		List<WebElement> cardsList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCardsPage/list_cards'), 60)
		println "Number of cards allocated are "+cardsList.size()
	}

	public static String cardNumber
	@Keyword
	public clickOnFirstCard() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCardsPage/link_firstCard'), 70)
		cardNumber=WebUI.getText(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCardsPage/link_firstCard'))
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCardsPage/link_firstCard'))
		WebUI.delay(10)
	}
}
