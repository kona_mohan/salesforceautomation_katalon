package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import com.helper.commonFunctions.JavaScriptFunctions

//import internal.GlobalVa/riable

public class CreatedCasePage_Salesforce_Header {

	@Keyword
	public String getCaseNumber() {
		JavaScriptFunctions.scrollUpByPixel('1000')
		WebUI.waitForElementHasAttribute(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Header/text_CaseNumber'), 'title', 50)
		println WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Header/text_CaseNumber'), 'title')
		return WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Header/text_CaseNumber'), 'title')
	}

	public static String GetStatus() {
		WebUI.delay(10)
		WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Header/text_Status'), 60)
		return WebUI.getText(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Header/text_Status'))
	}

	@Keyword
	public void verifyStatus(String status) {
		WebUI.delay(5)
		Assert.assertEquals(GetStatus(), status)
	}
}
