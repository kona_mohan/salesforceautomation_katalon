package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

import com.helper.commonFunctions.JavaScriptFunctions
public class CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCasePage_Salesforce {

	@Keyword
	public void clickNewButton() {
		WebUI.delay(5)
		try{
			if(WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedCasesPage/button_newCase'), 10))
			{
				WebUI.waitForElementPresent(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedCasesPage/button_newCase'), 60)
				WebUI.click(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedCasesPage/button_newCase'))
			}
			else
			{
				WebUI.refresh()
				WebUI.delay(10)
				WebUI.waitForElementPresent(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedCasesPage/button_newCase'), 60)
				WebUI.click(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedCasesPage/button_newCase'))
			}
		}catch(Exception e){}
		
	}
}
