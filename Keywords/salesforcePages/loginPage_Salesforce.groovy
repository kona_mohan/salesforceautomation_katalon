package salesforcePages
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

//import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.Keys

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class loginPage_Salesforce {

//	public static WebDriver driver;
	
	public static void setFireFoxPreferences() {
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.download.folderList",2);
		profile.setPreference("browser.download.dir", "C:\\Users\\MohanKo\\Downloads");
		profile.setPreference("browser.download.useDownloadDir", true);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/excel,text/csv,pdf");
		profile.setPreference("pdfjs.disabled", true);
		FirefoxDriver driver = new FirefoxDriver(profile)
		DriverFactory.changeWebDriver(driver)
	}

	@Keyword
	public void loginWithCredentials(String applicationURL, String Uname, String pwd) {
		WebUI.openBrowser(applicationURL)
		WebUI.maximizeWindow()
		WebUI.waitForPageLoad(10)
		WebUI.waitForElementVisible(findTestObject('OR_Salesforce/LoginPage_Salesforce/input_username'), 40)
		WebUI.sendKeys(findTestObject('OR_Salesforce/LoginPage_Salesforce/input_username'), Uname)
		WebUI.waitForElementVisible(findTestObject('OR_Salesforce/LoginPage_Salesforce/input_password'), 40)
		WebUI.sendKeys(findTestObject('OR_Salesforce/LoginPage_Salesforce/input_password'), pwd)
		WebUI.click(findTestObject('OR_Salesforce/LoginPage_Salesforce/button_login'))
	}
	
	
}