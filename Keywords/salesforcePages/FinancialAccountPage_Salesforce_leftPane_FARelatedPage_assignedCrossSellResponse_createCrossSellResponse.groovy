package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCrossSellResponse_createCrossSellResponse {
	
	public void selectFromDropDown(TestObject obj, String valuetoBeSelected) {
		List<WebElement> allElements = WebUiCommonHelper.findWebElements(obj, 20)
		for(int i=0;i<=allElements.size()-1;i++) {
			String dropDownValue=allElements.get(i).findElement(By.xpath(".//a")).getText()
			if(valuetoBeSelected.equalsIgnoreCase(dropDownValue)) {
				allElements.get(i).click()
				break
			}
		}
	}
	
	@Keyword
	public void selectCrosSellSuccess(String yesOrNo)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_crossSellResponse'), 20)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_crossSellResponse'), 20)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_crossSellResponse'))
		selectFromDropDown(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/commonDropDown'), yesOrNo)
	}
	
	
	@Keyword
	public void selectCountry(String country)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_country'), 20)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_country'), 20)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_country'))
		selectFromDropDown(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/commonDropDown'), country)
	}
	
	@Keyword
	public void selectServiceSold(String serviceSold)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_serviceSold'), 20)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_serviceSold'), 20)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_serviceSold'))
		selectFromDropDown(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/commonDropDown'), serviceSold)
	}
	
	
	
	@Keyword
	public void clickSave()
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_Save'), 30)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_Save'), 20)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/assignedCrossSellResponses/createCrossSellResponse/button_Save'))
	}
}
