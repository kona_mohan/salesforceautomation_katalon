package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject


import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.testng.Assert

import com.helper.commonFunctions.JavaScriptFunctions
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


public class CreatedCasePage_Salesforce_Body_CaseDetailsPage_ChangeOwnerPopUp_Salesforce {


	@Keyword
	public void EnterOwnerName(String ownerName) {
		WebUI.waitForElementVisible(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/input_OwnerName'), 60)
		if(!salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.getOwnerName.equalsIgnoreCase(ownerName)) {
			WebUI.sendKeys(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/input_OwnerName'), ownerName)
			WebUI.waitForPageLoad(5)
			JavaScriptFunctions.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/dropDown_ChangeOwnerName'))
			WebUI.waitForElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/button_ChangeOwner'), 60)
			WebUI.waitForElementClickable(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/button_ChangeOwner'), 60)
			WebUI.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/button_ChangeOwner'))
			WebUI.delay(1)
			salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.getOwnerName=salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.getCaseOwnerName()
			
		}
		else{
			println " "+salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.getOwnerName+" is already the owner"
			KeywordUtil.markErrorAndStop(ownerName+" already owns the case")
		}
	}

	@Keyword
	public void EnterQueue(String queue) {
		if(!salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.getOwnerName.equalsIgnoreCase(queue)) {
			WebUI.sendKeys(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/input_OwnerName'), queue)
			WebUI.delay(5)
			WebUI.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/dropDown_ChangeOwnerName'))
			WebUI.waitForElementPresent(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/button_ChangeOwner'), 60)
			WebUI.waitForElementClickable(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/button_ChangeOwner'), 60)
			WebUI.click(findTestObject('OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/button_ChangeOwner'))
			WebUI.delay(5)
			salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.getOwnerName=salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.getCaseOwnerName()
		}
		else {
			KeywordUtil.markErrorAndStop(salesforcePages.CreatedCasePage_Salesforce_Body_CaseDetailsPage_Salesforce.getOwnerName+"is already in the same queue")
		}
	}

	@Keyword
	public void changeOwnerType(String ownerType) {
		WebUI.delay(2)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/button_ChangeOwnerType'), 40)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/button_ChangeOwnerType'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/button_ChangeOwnerType'))
		WebUI.delay(1)
		List<WebElement> ownerTypeList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/ChangeOwnerPopUp_SalesForce/dropDown_ChangeOwnerType'), 40)
		for(int i=0;i<=ownerTypeList.size()-1;i++) {

			println ownerTypeList.get(i).findElement(By.xpath(".//a")).getAttribute("title")
			if(ownerTypeList.get(i).findElement(By.xpath(".//a")).getAttribute("title").contains(ownerType)) {
				ownerTypeList.get(i).findElement(By.xpath(".//a")).click()
			}
		}
	}

	//	@Keyword
	//	public
}
