package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


import com.helper.commonFunctions.JavaScriptFunctions

public class CreatedCasePage_SalesForce_Body {

	@Keyword
	public void clickOnFeedTab() {
		WebUI.delay(5)
		try{
			if(WebUI.verifyElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/tab_feed'))) {
				WebUI.refresh()
				WebUI.delay(5)
				WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/tab_feed'), 60)
				WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/tab_feed'), 60)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/tab_feed'))
			}
			else{
				WebUI.refresh()
				WebUI.delay(5)
				WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/tab_feed'), 60)
				WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/tab_feed'), 60)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/tab_feed'))
			}
		}catch(Exception e) {
		}
	}

	@Keyword
	public void clickOnRelatedTab() {
		WebUI.delay(2)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/tab_related'), 60)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/tab_related'), 60)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/tab_related'))
	}
}
