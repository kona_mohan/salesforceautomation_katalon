package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

import com.helper.commonFunctions.JavaScriptFunctions


public class MainAccountHolderPage_Salesforce_Body {

	@Keyword
	public void clickRelatedTab() {
		WebUI.delay(2)
		try{
			if(WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/MainAccountHolderPage_Salesforce/Body/tab_related'), 20)){
				WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/MainAccountHolderPage_Salesforce/Body/tab_related'), 70)
				JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/MainAccountHolderPage_Salesforce/Body/tab_related'))
			}
			else {
				WebUI.refresh()
				WebUI.delay(5)
				WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/MainAccountHolderPage_Salesforce/Body/tab_related'), 70)
				JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/MainAccountHolderPage_Salesforce/Body/tab_related'))
			}
		}
		catch(Exception e) {
		}
	}
}

