package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI



public class CustomerPage_Salesforce_Header_EditCustomerDetailsPage {
	
	@Keyword
	public void enterFirstName(String fName)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_firstName'), 40)
		WebUI.clearText(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_firstName'))
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_firstName'), fName) 
	}
	
	@Keyword
	public void enterLastName(String lName)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_lastName'), 40)
		WebUI.clearText(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_lastName'))
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_lastName'), lName)
	}
	
	@Keyword
	public void enterEmail(String email)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_email'), 40)
		WebUI.clearText(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_email'))
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_email'), email)
	}
	
	@Keyword
	public void enterMobileNumber(String mobileNo)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_mobileNumber'), 40)
		WebUI.clearText(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_mobileNumber'))
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_mobileNumber'), mobileNo)
	}
	
	@Keyword
	public void verifyEmailAndMobileNumberFieldsNotMandatory()
	{
		WebDriver driver = DriverFactory.getWebDriver()
		WebElement frame = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/iFrame'), 40)
		driver.switchTo().frame(frame)
		WebElement emailDiv =WebUiCommonHelper.findWebElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_email'), 40)
		String classNameEmail = emailDiv.findElement(By.xpath(".//ancestor::div[1]")).getAttribute("class")
		Assert.assertNotEquals(classNameEmail, "requiredInput")
		driver.switchTo().defaultContent()
		driver.switchTo().frame(frame)
		WebElement mobileDiv= WebUiCommonHelper.findWebElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/input_mobileNumber'), 40)
		String classNameMobile= mobileDiv.findElement(By.xpath(".//ancestor::div[1]")).getAttribute("class")
		Assert.assertNotEquals(classNameMobile, "requiredInput")
		driver.switchTo().defaultContent()
	}
	
	@Keyword
	public void clickOnSave()
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/button_save'), 40)
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/header/EditCustomerDetailsPage/button_save'))
		WebUI.delay(5)
	}
}
