package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable
import com.helper.commonFunctions.JavaScriptFunctions

public class FinancialAccountPage_Salesforce_leftPane_FARelatedPage_Salesforce {

	@Keyword
	public void clickOnCasesHeader() {
		
		try{
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/casesHeader'), 60)) {
				WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/casesHeader'), 60)
				JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/casesHeader'))
			}
			else {
				JavaScriptFunctions.scrollDownByPixel("1000")
				WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/casesHeader'), 60)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/casesHeader'))
			}
		}catch(Exception e) {
		}
	}

	@Keyword
	public void clickOnCardsHeader() {
		try{
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/cardsHeader'), 60)) {
				WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/cardsHeader'), 60)
				JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/cardsHeader'))
			}
			else {
				JavaScriptFunctions.scrollDownByPixel("1000")
				WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/cardsHeader'), 60)
				JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/cardsHeader'))
			}
		}catch(Exception e) {
		}
	}
	
	@Keyword
	public void clickOnCrossSellResponse()
	{
		WebUI.delay(2)
		JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/leftPane/FARelatedPage/crossSellRsponseHeader'))
	}
}
