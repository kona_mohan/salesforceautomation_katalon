package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

import salesforcePages.AllCasesPage_SalesForce

public class CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedCasePage_CaseCreationPage_Salesforce {

	@Keyword
	public void verifyCustomerName() {
		WebUI.delay(15)
		WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedCasesPage/CaseCreationPage_Salesforce/text_CustomerName'), 10)
		String customerName= WebUI.getText(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedCasesPage/CaseCreationPage_Salesforce/text_CustomerName'))
		println customerName
		Assert.assertEquals(customerName, AllCasesPage_SalesForce.FirstValueIn)
	}
}
