package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class FinancialAccountPage_Salesforce_leftPane_FARelatedPage_assignedCasesPage_CaseCreationPage_Salesforce {

	@Keyword
	public void verifyCustomerName() {
		WebUI.delay(15)
		WebUI.verifyElementPresent(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedCasesPage/CaseCreationPage_Salesforce/text_FinancialAccount'), 10)
		String FAName= WebUI.getText(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedCasesPage/CaseCreationPage_Salesforce/text_FinancialAccount'))
		println FAName
		Assert.assertEquals(FAName,AllCasesPage_SalesForce.FirstValueIn)
	}
}
