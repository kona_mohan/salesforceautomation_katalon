package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.testng.Assert

import com.helper.commonFunctions.JavaScriptFunctions
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class FinancialAccountPage_Salesforce_rightPane_ConsentsDetails {
	
	@Keyword
	public void clickShowConsent()
	{
		try{
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_showConsents'), 10))
			{
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_showConsents'))
			}
			else{
				WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_showConsents'), 30)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_showConsents'))
			}
		}catch(Exception e){}
	}
	
	@Keyword
	public void verifySuccessMessageInGreen()
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/text_SuccessConsent'), 10)
		WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/text_SuccessConsent'), 10)
	}
	@Keyword
	public void clickUpdateConsent()
	{
		try{
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_update'), 10))
			{
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_update'))
			}
			else{
				WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_update'), 30)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_update'))
			}
		}catch(Exception e){}
		
	}
	@Keyword
	public void clickGetHistoricalConsent()
	{
		try{
			
			if(WebUI.verifyElementVisible(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_getHistoricalConsent')))
			{
				JavaScriptFunctions.scrollDownByPixel("100")
				WebUI.delay(2)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_getHistoricalConsent'))
			}
			else{
				println "in else"
				WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_getHistoricalConsent'), 30)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_getHistoricalConsent'))
			}
		}catch(Exception e){}
		
	}
	
	
	@Keyword
	public void selectConsentChannels(String consent)
	{
		WebUI.switchToFrame(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/iFrame'), 40)
		List<WebElement> consentList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/list_consentChannel'), 30)
		for(int i=0;i<=consentList.size()-1;i++)
		{
			println i
			if(consentList.get(i).isDisplayed() && consentList.get(i).findElement(By.xpath(".//label")).getText().equalsIgnoreCase(consent))
			{
				println "in if"
				consentList.get(i).findElement(By.xpath(".//input")).click()
				break
			}
		}
		WebUI.switchToDefaultContent()
	}
	
	@Keyword
	public void fetchConsentHistory(String fromDate, String toDate)
	{
		WebUI.switchToWindowIndex(1)
		WebUI.maximizeWindow()
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/input_fromDateHistoricalConsent'), 30)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/input_fromDateHistoricalConsent'), fromDate)
		WebUI.delay(2)
//		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/input_fromDateHistoricalConsent'), Keys.chord(Keys.TAB))
//		WebUI.delay(2)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/input_toDateHistoricalConsent'), toDate)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/input_toDateHistoricalConsent'), Keys.chord(Keys.TAB))
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/button_getConsentHistory'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/list_consentHistory'), 10)
		println WebUiCommonHelper.findWebElements(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/list_consentHistory'),20).isEmpty()
		Assert.assertFalse(WebUiCommonHelper.findWebElements(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/ConsentsDetails/list_consentHistory'),20).isEmpty())
	}
}
