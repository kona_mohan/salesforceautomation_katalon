package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


import com.helper.commonFunctions.JavaScriptFunctions
public class CreatedCasePage_Salesforce_Center {


	@Keyword
	public void clickAcceptButton(String ownerName) {
		WebUI.refresh()
		WebUI.delay(3)
		if(ownerName.equalsIgnoreCase(WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseDetailsPage_Salesforce/text_ownerName'), "innerText"))){
			KeywordUtil.markError(ownerName +" already owns the case")
		}
		else{
			JavaScriptFunctions.scrollUpByPixel("3000")
			WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Center/button_accept'), 60)
			WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Center/button_accept'))
			WebUI.delay(2)
		}

	}

	@Keyword
	public void clickPrimeActionButton() 
	{
		WebUI.delay(2)
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Header/text_Status'), 10)
					JavaScriptFunctions.scrollUpByPixel("3000")
					WebUI.delay(2)
		try{
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Center/button_primeAction'),30)) {
//				WebUI.delay(3)
				WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Header/text_CaseNumber'), 30)
				WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Center/button_primeAction'), 40)
				JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Center/button_primeAction'))
			}
			else{
				WebUI.refresh()
				WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Header/text_CaseNumber'), 30)
				WebUI.delay(2)
				JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Center/button_primeAction'))
			}
		}catch(Exception e) {
		}
	}
}
