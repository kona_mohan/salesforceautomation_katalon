package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.helper.commonFunctions.JavaScriptFunctions

//import internal.GlobalVariable

public class FinancialAccountPage_Salesforce_rightPane_Salesforce {

	@Keyword
	public void clickOnLoanDetailsTab() {
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_FAstatusAndBalance'), 60)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_loanDetails'), 60)
		String tabOpen= WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_loanDetails'), "aria-expanded")
		if(tabOpen.equalsIgnoreCase("false")) {
			WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_loanDetails'))
		}
	}

	@Keyword
	public void clickOnFAstatusAndBalanceTab() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_FAstatusAndBalance'), 60)
		String tabOpen= WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_FAstatusAndBalance'), "aria-expanded")
		if(tabOpen.equalsIgnoreCase("false")) {
			WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_FAstatusAndBalance'))
		}
	}

	@Keyword
	public void clickOnStatementHistoryTab() {
		try{
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_statementHistory'), 60)) {
				//				JavaScriptFunctions.scrollDownByPixel("1000")
				println"in if"
				String tabOpen= WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_statementHistory'), "aria-expanded")
				if(tabOpen.equalsIgnoreCase("false")) {
					JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_statementHistory'))
				}
			}
			else {
				println"in else"
				JavaScriptFunctions.scrollDownByPixel("1000")
				String tabOpen= WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_statementHistory'), "aria-expanded")
				if(tabOpen.equalsIgnoreCase("false")) {
					JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_statementHistory'))
				}
			}
		}catch(Exception e) {
		}
	}
	
	@Keyword
	public void clickOnConsentsTab() {
		try{
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_consents'), 60)) {
				//				JavaScriptFunctions.scrollDownByPixel("1000")
				println"in if"
				String tabOpen= WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_consents'), "aria-expanded")
				if(tabOpen.equalsIgnoreCase("false")) {
					JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_consents'))
				}
			}
			else {
				println"in else"
				JavaScriptFunctions.scrollDownByPixel("1000")
				String tabOpen= WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_consents'), "aria-expanded")
				if(tabOpen.equalsIgnoreCase("false")) {
					JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/tab_consents'))
				}
			}
		}catch(Exception e) {
		}
	}
}
//tab_consents