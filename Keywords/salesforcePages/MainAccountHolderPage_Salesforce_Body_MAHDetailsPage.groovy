package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.testng.Assert
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class MainAccountHolderPage_Salesforce_Body_MAHDetailsPage {

	@Keyword
	public void verifyFieldsInDetailsPage() {
		WebDriver driver = DriverFactory.getWebDriver()
		ArrayList<String> fieldsToBeVerified =new ArrayList<String>()
		fieldsToBeVerified.add("Main Account Holder Name")
		fieldsToBeVerified.add("Product")
		fieldsToBeVerified.add("Main Account Holder Serno")
		fieldsToBeVerified.add("Product Serno")
		int count=0;
		List<WebElement> FieldsInDetailsPage = driver.findElements(By.xpath("//*[@class='row row-main column']//*[@class='slds-form form-horizontal ']/div"))
		for(int i=0;i<=FieldsInDetailsPage.size()-2;i++) {
			for(int j=1;j<=FieldsInDetailsPage.size()-1;j++) {
				String fieldText=FieldsInDetailsPage.get(i).findElement(By.xpath(".//div["+(j)+"]//span[@class='test-id__field-label'][1]")).getText()
				Assert.assertEquals(fieldText, fieldsToBeVerified.get(count))
				count++
			}
		}
	}
}
