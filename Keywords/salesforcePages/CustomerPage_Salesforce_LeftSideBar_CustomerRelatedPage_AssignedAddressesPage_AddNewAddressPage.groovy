package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.helper.commonFunctions.JavaScriptFunctions
import com.helper.commonFunctions.waitStatements

//import internal.GlobalVariable

public class CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage_AssignedAddressesPage_AddNewAddressPage {

	@Keyword
	public void verifyTypeOfAddressIsTextBox(String tagName) {
		WebUI.delay(5)
		WebUI.waitForElementVisible(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_TypeOfAddress'), 5)
		Assert.assertEquals(WebUiCommonHelper.findWebElement(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_TypeOfAddress') , 60).getTagName(),tagName)
	}

	@Keyword
	public void enterTypeOfAddress(String typeOfAddress) {
		WebUI.delay(5)
		WebUI.waitForElementVisible(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_TypeOfAddress'), 5)
		WebUI.sendKeys(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_TypeOfAddress'), typeOfAddress)
	}

	@Keyword
	public void verifyStreetAddressMaxLength(String maxLength) {
		WebUI.waitForElementClickable(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_streetAddress'), 10)
		String addressLength = WebUI.getAttribute(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_streetAddress'), "maxlength")
		println addressLength
		Assert.assertEquals(WebUI.getAttribute(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_streetAddress'), "maxlength"), maxLength)
	}

	@Keyword
	public void enterStreetAddress(String streetAddress) {
		WebUI.delay(5)
		WebUI.waitForElementVisible(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_streetAddress'), 5)
		WebUI.sendKeys(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_streetAddress'), streetAddress)
	}

	@Keyword
	public void verifyCityMaxLength(String maxLength) {
		WebUI.waitForElementVisible(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_city'), 60)
		Assert.assertEquals(maxLength, WebUI.getAttribute(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_city'), "maxlength"))
	}
	
	@Keyword
	public void enterCity(String city) {
		WebUI.waitForElementVisible(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_city'), 60)
		WebUI.sendKeys(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_city'), city)
	}

	@Keyword
	public void verifyZipMaxLength(String maxLength) {
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_zip'), 50)
		Assert.assertEquals(WebUI.getAttribute(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_zip'), "maxlength"), maxLength)
	}

	@Keyword
	public void enterZipCode(String zipcode) {
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_zip'), 50)
		WebUI.sendKeys(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/input_zip'), zipcode)
	}

	@Keyword
	public void verifyCountryAutoFilled() {
		Assert.assertNotNull(WebUI.getText(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/text_country')))
	}

	@Keyword
	public void clickOnSave() {
		WebUI.click(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/assignedAddressesPage/addNewAddressPage/button_save'))
	}
}
