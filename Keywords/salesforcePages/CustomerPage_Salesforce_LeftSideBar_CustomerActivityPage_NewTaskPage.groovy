package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


public class CustomerPage_Salesforce_LeftSideBar_CustomerActivityPage_NewTaskPage {
	
	@Keyword
	public void setSubject(String subject)
	{
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/tab_Activity'), 20)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerActivityPage/NewTaskTab/input_subject'), 30)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerActivityPage/NewTaskTab/input_subject'), subject)
	}
	
	@Keyword
	public void setDate()
	{
//		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerActivityPage/NewTaskTab/button_date'), 20)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerActivityPage/NewTaskTab/button_date'), 30)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerActivityPage/NewTaskTab/button_date'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerActivityPage/NewTaskTab/span_currentDate'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerActivityPage/NewTaskTab/span_currentDate'))
	}
	
	@Keyword
	public void clickSave()
	{
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/tab_Activity'), 20)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerActivityPage/NewTaskTab/button_SAVE'), 20)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerActivityPage/NewTaskTab/button_SAVE'))
	}
}
