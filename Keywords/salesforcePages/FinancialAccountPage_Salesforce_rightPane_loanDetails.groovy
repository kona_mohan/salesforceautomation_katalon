package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class FinancialAccountPage_Salesforce_rightPane_loanDetails {
	
	@Keyword
	public void clickOnGetLoanDetailsButton()
	{
		WebUI.delay(5)
		WebDriver driver = DriverFactory.getWebDriver()
		driver.switchTo().frame(driver.findElement(By.xpath('//*[@class="column right-col"]/*[@class="flexipageComponent"]//li[@title="Loan Details"]//following::iframe')))
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/LoanDetails/button_getLoanDetails'), 60)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/rightPane/LoanDetails/button_getLoanDetails'))
	}
}
