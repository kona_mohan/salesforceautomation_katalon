package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import com.helper.commonFunctions.JavaScriptFunctions
import com.helper.commonFunctions.ActionClassFunctions
//import internal.GlobalVariable

public class CustomerPage_Salesforce_LeftSideBar_CustomerRelatedPage {


	public String returnCaseCount() {
		return WebUI.getText(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/txt_caseCountInHeader'))
	}

	@Keyword
	public void verifyCaseCountallocatedToCustomer() {
		WebUI.delay(10)
		String caseCount = returnCaseCount()
		println caseCount
		StringBuilder build = new StringBuilder()
		int count = Integer.parseInt(caseCount.substring(1,2))
		println count
		if(count>0) {
			Assert.assertEquals(count, count)
		}
		else
			println "No cases has been allocated"
	}

	@Keyword
	public void clickOnCasesHeader() {
		WebUI.delay(5)
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/txt_caseCountInHeader'), 60)
		WebUI.click(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/txt_caseCountInHeader'))
	}

	@Keyword
	public void clickOnCardsHeader() {
		WebUI.delay(2)
		JavaScriptFunctions.scrollToElement(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_cards'))
		JavaScriptFunctions.click(findTestObject('OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_cards'))
	}



	@Keyword
	public void clickOnAddressesHeader() {
		try{
			if(WebUI.verifyElementVisible(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_addresses'), 60)) {
				println "clicking header"
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_addresses'))
			}
		}catch(Exception e) {
			WebUI.delay(5)
			println "scrolling down and clicking"
			WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/txt_caseCountInHeader'), 60)
			WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_addresses'), 50)
			WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_addresses'))
		}
	}

	@Keyword
	public void clickOnCrossSellHeader()
	{
		WebUI.delay(2)
		JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_crossSell'))
	}
	
	@Keyword
	public void clickOnMAHHeader() {
		JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_MAH'))
		//		try{
		//
		//			if(WebUI.verifyElementVisible(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_MAH'), 10)) {
		//				println "clicking header"
		//				WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_MAH'))
		//			}
		//		}catch(Exception e) {
		//			println "else method"
		//			WebUI.waitForPageLoad(5)
		//			WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_customerHistory'), 10)
		//			WebUI.delay(3)
		//			WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_MAH'), 50)
		//			WebUI.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_MAH'))
		//		}
	}

	@Keyword
	public void clickOnAlertHeader() {
		WebUI.delay(5)
		try{
			if(WebUI.verifyElementVisible(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_alerts'), 10)) {
				println "clicking header"
				JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_alerts'))
			}
		}catch(Exception e) {
			WebUI.delay(5)
			println "scrolling down and clicking"
			JavaScriptFunctions.scrollDownByPixel("100")
			WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_alerts'), 50)
			JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerRelatedPage/link_alerts'))
		}
	}
}
