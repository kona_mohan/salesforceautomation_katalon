package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import com.helper.commonFunctions.JavaScriptFunctions

//import internal.GlobalVariable

public class MainAccountHolderPage_Salesforce_Body_MAHRelatedPage {

	public String returnCaseCount() {
		JavaScriptFunctions.scrollDownByPixel('1000')
		WebUI.delay(5)
		return WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/MainAccountHolderPage_Salesforce/Body/MAHRelatedPage/txt_caseCountInHeader'),'title')
	}

	public static String caseCount
	public static int count
	@Keyword
	public void verifyCaseCountallocatedToMAH() {
		WebUI.delay(5)
//		println "mohan"
		caseCount = returnCaseCount()
		println caseCount
		StringBuilder build = new StringBuilder()
		count = Integer.parseInt(caseCount.substring(1,2))
		println count
		if(count>0) {
			Assert.assertEquals(count, count)
		}
		else
			println "No cases has been allocated"
	}
}
