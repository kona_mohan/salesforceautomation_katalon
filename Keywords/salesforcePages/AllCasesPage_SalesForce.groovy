package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.Robot
import java.awt.event.KeyEvent

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.testng.Assert

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptFunctionJob
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI



import com.helper.commonFunctions.JavaScriptFunctions

public class AllCasesPage_SalesForce {

	@Keyword
	public void clickNewButton() {
		WebUI.delay(5)
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/AllCasesPage_SalesForce/button_newCase'), 30)
		WebUI.waitForElementClickable(findTestObject('OR_Salesforce/AllCasesPage_SalesForce/button_newCase'), 50)
		WebUI.click(findTestObject('OR_Salesforce/AllCasesPage_SalesForce/button_newCase'))
	}


	@Keyword
	public void selectCaseNumberFromTable(String caseNumber) {
		WebUI.refresh()
		WebUI.delay(5)
		List<WebElement> caseNumbersList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/list_CaseNumberInTable'), 60)
		println caseNumbersList.size()
		for(int i=0;i<=caseNumbersList.size();i++) {
				if(caseNumbersList.get(i).displayed) {					
					WebElement caseNumberClicked =caseNumbersList.get(i).findElement(By.xpath(".//a"))
					String CaseNumberInList = caseNumberClicked.getText()
					println CaseNumberInList
					if(CaseNumberInList.contains(caseNumber)) {
						println "clicking"
						caseNumberClicked.click()
						break
					}
				}
		
		}
	}


	public void selectCaseWithStatusNewOrProgress() {
		WebUI.delay(5)
		boolean flag=true
		List<WebElement> casesList = WebUiCommonHelper.findWebElements(findTestObject('OR_Salesforce/AllCasesPage_SalesForce/list_CasesInTable'), 60)

		for(int i=0;i<=casesList.size()-1;i++) {

			String caseOwner= casesList.get(i).findElement(By.xpath(".//td[6]//span[@title]")).getAttribute("title")
			println caseOwner
			for(int j=0;j<=casesList.size()-1;j++) {

				if(!caseOwner.equalsIgnoreCase("kona.moh")) {
					String caseStatus = casesList.get(j).findElement(By.xpath(".//td[5]/span/span")).getText()
					println caseStatus
					if(caseStatus.equalsIgnoreCase("New") || caseStatus.equalsIgnoreCase("In Progress")) {
						println "clicking"
						WebUI.delay(5)
						casesList.get(i).findElement(By.xpath(".//th//a[@class]")).click()
						flag=false
						break
					}
					if(!flag)
						break
				}
			}
			if(!flag)
				break
		}
	}

	public static String FirstValueIn
	@Keyword
	public String clickOnFirstOpenCaseInList() {
		WebUI.delay(5)
		boolean flag=true
		List<WebElement> casesList = WebUiCommonHelper.findWebElements(findTestObject('OR_Salesforce/AllCasesPage_SalesForce/list_CasesInTable'), 60)
		for(int i=0;i<=casesList.size()-1;i++) {
			String caseStatus = casesList.get(i).findElement(By.xpath(".//td[5]/span/span")).getText()
			println caseStatus
			if(!caseStatus.equalsIgnoreCase("Closed")) {
				println "clicking"
				WebUI.delay(5)
				FirstValueIn=casesList.get(i).findElement(By.xpath(".//th//a[@class]")).getAttribute("title")
				casesList.get(i).findElement(By.xpath(".//th//a[@class]")).click()
				flag=false
				break
			}
		}
		return FirstValueIn
	}

	@Keyword
	public String clickOnFirstCaseInList() {
		WebUI.delay(3)
		try{
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/link_firstCaseInList'), 5))
			{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/link_firstCaseInList'), 40)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/link_firstCaseInList'), 40)
		FirstValueIn=WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/link_firstCaseInList'), "title")
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/link_firstCaseInList'))
		return FirstValueIn
			}else{
			WebUI.refresh()
			WebUI.delay(2)
			WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/link_firstCaseInList'), 40)
			WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/link_firstCaseInList'), 40)
			FirstValueIn=WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/link_firstCaseInList'), "title")
			WebUI.click(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/link_firstCaseInList'))
			return FirstValueIn
			}
	}catch(Exception e){}
	}

	@Keyword
	public void clickOnCasewithOwnerName(String ownername) {
		WebUI.delay(5)
		List<WebElement> casesList = WebUiCommonHelper.findWebElements(findTestObject('OR_Salesforce/AllCasesPage_SalesForce/list_CasesInTable'), 60)
		for(int i=0;i<=casesList.size()-1;i++) {

			String caseOwner= casesList.get(i).findElement(By.xpath(".//td[6]//span[@title]")).getAttribute("title")
			if(caseOwner.equalsIgnoreCase(ownername)) {
				casesList.get(i).findElement(By.xpath(".//th//a[@class]")).click()
				break
			}
		}
	}

	@Keyword
	public void clickOnCasewithSSNNumber() {
		WebUI.delay(5)
		List<WebElement> casesList = WebUiCommonHelper.findWebElements(findTestObject('OR_Salesforce/AllCasesPage_SalesForce/list_CasesInTable'), 60)
		for(int i=0;i<=casesList.size()-1;i++) {

			String ssnNumber= casesList.get(i).findElement(By.xpath(".//td[8]//span[@title]")).getAttribute("title")
			if(!ssnNumber.isEmpty()) {
				casesList.get(i).findElement(By.xpath(".//th//a[@class]")).click()
				break
			}
		}
	}

	@Keyword
	public void clickOnCaseWithStatusNewOrProgress() {
		selectCaseWithStatusNewOrProgress()
	}

	@Keyword
	public void selectSubCategory(String subCategory) {
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/button_subCategory'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/button_subCategory'))
		WebUI.delay(2)
//		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/input_searchBoxSubCategory'), 40)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/input_searchBoxSubCategory'), subCategory)
		WebUI.delay(3)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/a_subCategoryInDropDown'), 30)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/a_subCategoryInDropDown'), 30)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/a_subCategoryInDropDown'))
	}


	@Keyword
	public void verifyCaseInDKFraudAndChargeback(String caseNumber) {
		WebUI.delay(5)
		List<WebElement> casesList = WebUiCommonHelper.findWebElements(findTestObject('OR_Salesforce/AllCasesPage_SalesForce/list_CasesInTable'), 60)
		println casesList.size()
		for(int i=0;i<=casesList.size()-1;i++) {

			String caseNumberInList= casesList.get(i).findElement(By.xpath(".//td[3]//a")).getAttribute("title")
			if(caseNumber.equalsIgnoreCase(caseNumberInList)) {
				Assert.assertTrue(true)
			}
		}
	}

	@Keyword
	public void clickChangeOwnerInDKFraudAndChargeback(String caseNumber) {
		List<WebElement> casesList = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/list_CasesInTable'), 60)
		for(int i=0;i<=casesList.size()-1;i++) {
			String caseNumberInList= casesList.get(i).findElement(By.xpath(".//td[3]//a")).getAttribute("title")
			if(caseNumber.equalsIgnoreCase(caseNumberInList)) {
				println "clicking change owner"
				casesList.get(i).findElement(By.xpath(".//td[10]//a")).click()
				WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/button_dropDownChangeOwner'), 40)
				WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/button_dropDownChangeOwner'), 30)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/AllCasesPage_SalesForce/button_dropDownChangeOwner'))
			}
		}
	}
}
