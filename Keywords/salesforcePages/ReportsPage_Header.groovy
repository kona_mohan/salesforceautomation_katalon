package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.Robot
import java.awt.event.KeyEvent

import org.openqa.selenium.Keys

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class ReportsPage_Header {

	@Keyword
	public void searchReport(String reportName) {
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/header/input_searchBar'), 40)
		WebUI.clearText(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/header/input_searchBar'))
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/header/input_searchBar'), reportName)
		WebUI.delay(1)
		Robot rb = new Robot()
		rb.keyPress(KeyEvent.VK_BACK_SPACE)
		WebUI.delay(1)
		rb.keyPress(KeyEvent.VK_BACK_SPACE)
		WebUI.delay(1)
		rb.keyPress(KeyEvent.VK_BACK_SPACE)
	}
}
