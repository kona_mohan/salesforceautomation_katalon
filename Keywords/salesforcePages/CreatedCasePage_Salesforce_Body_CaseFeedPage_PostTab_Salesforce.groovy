package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI



public class CreatedCasePage_Salesforce_Body_CaseFeedPage_PostTab_Salesforce {

	@Keyword
	public void sendPost(String postText) {
		WebUI.delay(10)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/text_postText'), 60)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/text_postText'))
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/text_postText'), postText)
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/button_Share'), 60)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/button_Share'), 60)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/button_Share'))
	}

	public static String verifyText;
	@Keyword
	public void verifyPostInAllUpdates(String postText) {
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/text_allUpdatesList'), 60)
		WebUI.delay(2)

		try{
			WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/text_allUpdatesListOpen'), 10)
			verifyText=WebUI.getText(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/text_allUpdatesListOpen'))
		}catch(Exception e) {

			WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/text_allUpdatesList'), 10)
			verifyText=WebUI.getText(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/text_allUpdatesList'))
		}
		Assert.assertEquals(postText, verifyText)
	}
}
