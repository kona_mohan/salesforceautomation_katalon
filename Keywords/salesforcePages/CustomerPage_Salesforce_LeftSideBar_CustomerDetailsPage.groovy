package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class CustomerPage_Salesforce_LeftSideBar_CustomerDetailsPage {

	public static String mobileNumber
	@Keyword
	public void getMobileNumber() {
		WebUI.delay(5)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerDetailsPage/text_mobileNumber'), 30)
		mobileNumber= WebUI.getText(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerDetailsPage/text_mobileNumber'))
		println mobileNumber
	}
	
	public static String serNo
	@Keyword
	public String getSerNo() {
		WebUI.delay(5)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerDetailsPage/text_serno'), 30)
		serNo= WebUI.getText(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerDetailsPage/text_serno'))
		println serNo
		return serNo
	}

	@Keyword
	public void verifyErrorMessageIfMobileNumberNotPresent() {
		try{
			if(mobileNumber.equalsIgnoreCase(null)) {
				println "mobile number not present"
				Assert.assertTrue(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerDetailsPage/text_errorMessageMobile'), 10))
			}
			else {
				println "mobile number present"
				Assert.assertTrue(WebUI.verifyElementNotPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerDetailsPage/text_errorMessageMobile'), 10))
			}
		}catch(Exception e) {
		}
	}

	@Keyword
	public void verifyAlertImagePresent() {
		WebUI.refresh()
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerDetailsPage/text_mobileNumber'), 30)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerDetailsPage/img_alert'), 20)
		Assert.assertTrue(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/CustomerPage_Salesforce/leftSideBar/CustomerDetailsPage/img_alert'), 20))
	}
}
