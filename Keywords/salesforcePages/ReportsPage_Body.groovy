package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.Robot
import java.awt.event.KeyEvent

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class ReportsPage_Body {


	public File getLatestFilefromDir(String dirPath){
		File dir = new File(dirPath);
		File[] files = dir.listFiles(new FilenameFilter() {

					//apply a filter
					@Override
					public boolean accept(File dir1, String name) {
						boolean result;
						if(name.toLowerCase().endsWith(".xlsx")){
							result=true;
						}
						else{
							result=false;
						}
						return result;
					}
				})

		if (files == null || files.length == 0) {
			return null;
		}


		File lastModifiedFile = files[0];
		for (int i = 1; i < files.length; i++) {
			if (lastModifiedFile.lastModified() < files[i].lastModified()) {
				lastModifiedFile = files[i];
			}
		}
		return lastModifiedFile;
	}
	@Keyword
	public void clickOnFirstReport() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/body/link_firstReport'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/body/link_firstReport'))
	}

	@Keyword
	public void clickOnFirstReportDropDownButton() {
		WebUI.delay(2)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/body/button_firstReport'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/body/button_firstReport'))
	}

	@Keyword
	public void clickOnExportInDropDown() {
		WebUI.delay(2)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/body/dropDown_Export'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/body/dropDown_Export'))
	}

	@Keyword
	public void clickOnExportButtonInPopUp()
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/body/button_exportPopup'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/body/button_exportPopup'))
		try{
			WebUI.delay(5)
			Robot rb = new Robot()
			rb.keyPress(KeyEvent.VK_ENTER)
			WebUI.delay(10)
			rb.keyPress(KeyEvent.VK_ENTER)
		}catch(Exception e)
		{

		}
	}

	@Keyword
	public void verifyExcelFileCreatedInLocalDriver(String path, String folderName)
	{
		WebUI.delay(10)
		File getLatestFile = getLatestFilefromDir(path);
		println getLatestFile
		String fileName = getLatestFile.getName();
		println fileName
		Assert.assertTrue(fileName.contains(folderName))
	}

	@Keyword
	public void verifyReportPresent(String reportName)
	{
		try
		{
			List<WebElement> allReportNames = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/OR_Salesforce/ReportsPage_Salesforce/body/link_allReports'), 30)
			for(int i=0;i<=allReportNames.size()-1;i++)
			{
				String ReportNameInList = allReportNames.get(i).findElement(By.xpath(".//th//a")).getAttribute("title")
				if(ReportNameInList.contains(reportName))
				{
					println "Report is available"
				}
				else
				{
					println "Report is not available"
				}
			}
		}catch(Exception e)
		{

		}
	}
}
