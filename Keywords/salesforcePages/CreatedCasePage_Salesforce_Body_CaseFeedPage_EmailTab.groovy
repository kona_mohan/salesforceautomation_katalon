package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.helper.commonFunctions.JavaScriptFunctions
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


public class CreatedCasePage_Salesforce_Body_CaseFeedPage_EmailTab {

	@Keyword
	public void clickOnAddEmail() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/button_AddEmail'), 60)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/button_AddEmail'))
		WebUI.delay(2)
	}

	@Keyword
	public void enterToRecipient(String recipient) {
		WebUI.delay(5)
		try {
			if(!WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/input_ToReciept'),10) {
				WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/input_ToReciept'), recipient)
			}
		}catch(Exception e) {
		}
	}

	@Keyword
	public void enterSubject(String subject) {
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/input_Subject'), 60)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/input_Subject'), subject)
	}

	@Keyword
	public void enterTextInBody(String matter) {
		WebUI.delay(2)
//		WebUI.switchToDefaultContent()
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/input_EmailBody'), 60)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/input_EmailBody'), 60)
		WebUI.doubleClick(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/input_EmailBody'))
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/input_EmailBody'), matter)
	}

	@Keyword
	public void clickSendMail() {
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/button_SendMail'), 60)
		WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/button_SendMail'), 60)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/button_SendMail'))
	}
	
	@Keyword
	public void clickOnInsertTemplate() {
		WebUI.switchToDefaultContent()
		WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/PostTab/link_AllUpdates'), 60)
		WebUI.delay(3)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/button_emailTemplate'), 60)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/button_emailTemplate'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/dropdown_insertTemplate'), 40)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/dropdown_insertTemplate'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseFeedPage_Salesforce/EmailTab/dropdown_insertTemplate'))
	}
}
