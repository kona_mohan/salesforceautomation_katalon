package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.helper.commonFunctions.JavaScriptFunctions
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import internal.GlobalVariable

public class FinancialAccountPage_Salesforce_centerPane {
	
	@Keyword
	public void clickOnPrimeAction()
	{
		WebUI.delay(2)
		JavaScriptFunctions.scrollUpByPixel("3000")
		try{
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/centerPane/button_primeAction'),30)) {
				WebUI.delay(3)
				WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/centerPane/button_primeAction'), 40)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/centerPane/button_primeAction'))
			}
			else{
				WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/centerPane/button_primeAction'), 0)
				WebUI.delay(3)
				WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/centerPane/button_primeAction'), 40)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/FinancialAccountPage/centerPane/button_primeAction'))
			
			}
		}catch(Exception e) {
		}
	}
}
