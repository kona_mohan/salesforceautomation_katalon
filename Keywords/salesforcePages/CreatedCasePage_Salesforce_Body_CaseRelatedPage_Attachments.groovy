package salesforcePages

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptFunctionJob
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


import com.helper.commonFunctions.JavaScriptFunctions
public class CreatedCasePage_Salesforce_Body_CaseRelatedPage_Attachments {

	@Keyword
	public void uploadRequiredAttachment(docPath) {
		WebUI.delay(5)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/button_UploadDoc'), 30)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/button_UploadDoc'), docPath)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/button_DoneUploading'), 50)
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/button_DoneUploading'))
		try{
			WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/button_DoneUploading'))
		}catch(Exception e) {
		}
	}

	@Keyword
	public void clickOnViewAttachments() {
		try{
			if(WebUI.verifyElementClickable(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/link_ViewAllAttachments'))) {
				WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/link_ViewAllAttachments'), 40)
				JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/link_ViewAllAttachments'))
			}
			else{
				JavaScriptFunctions.scrollDownByPixel("500")
				WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/link_ViewAllAttachments'), 40)
				JavaScriptFunctions.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/link_ViewAllAttachments'))
			}
		}catch(Exception e){}
	}

	@Keyword
	public void clickOnDeleteAttachment() {
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/button_dropDownAttachment'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/button_dropDownAttachment'))
		WebUI.delay(3)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/dropdown_delete'), 30)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/dropdown_delete'))
		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/CreatedCasePage_Salesforce/Body/CaseRelatedPage_Salesforce/Attachments/button_confirmDeleteAttachment'))
	}
}
