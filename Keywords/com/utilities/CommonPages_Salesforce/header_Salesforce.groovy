package com.utilities.CommonPages_Salesforce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.Robot
import java.awt.event.KeyEvent

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.helper.commonFunctions.JavaScriptFunctions
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


//import internal.GlobalVariable

public class header_Salesforce {

	//	public WebDriver driver = DriverFactory.getWebDriver()

	public void selectFromDropDown(TestObject obj, String valuetoBeSelected)
	{
		//		WebUI.delay(2)
		List<WebElement> allElements = WebUiCommonHelper.findWebElements(obj, 20)
		for(int i=0;i<=allElements.size()-1;i++)
		{
			String dropDownValue=allElements.get(i).findElement(By.xpath(".//a")).getText()
			if(valuetoBeSelected.equalsIgnoreCase(dropDownValue))
			{
				allElements.get(i).click()
				break
			}
		}

	}

	@Keyword
	public void searchInSearchBox(String input)
	{
		WebUI.delay(2)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/input_searchbar'), 50)
		WebUI.clearText(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/input_searchbar'))
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/input_searchbar'), input)
		WebUI.delay(2)
		Robot robot = new Robot()
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		WebUI.delay(3)
	}

	@Keyword
	public void searchAndSelectInSearchBox(String valueToBeSearched)
	{
		WebUI.delay(2)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/input_searchbar'), 50)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/input_searchbar'))
		WebUI.clearText(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/input_searchbar'))
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/input_searchbar'), valueToBeSearched)
		WebUI.delay(1)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/input_searchbar'), 30)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/input_searchbar'))
		WebUI.waitForPageLoad(5)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/dropDown_FirstElementInSearchBar'), 50)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/dropDown_FirstElementInSearchBar'), 30)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/dropDown_FirstElementInSearchBar'))
	}

	@Keyword
	public void verifyfilterOptionPresent()
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/dropdown_filterSearch'), 50)
		WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/dropdown_filterSearch'), 50)
	}


	@Keyword
	public void selectItemFromNavigationMenu(String menuItem) {
		WebUI.delay(8)
		try{
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/button_NavigationMenu'), 2))
			{
				WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/dropDown_NavigationMenu'), 2)
				WebUI.click(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/button_NavigationMenu'))
				selectFromDropDown(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/dropDown_NavigationMenu'),menuItem)
			}
		}catch(Exception e)
		{
			WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/button_NavigationMenu'), 50)
			WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/button_NavigationMenu'), 50)
			WebUI.click(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/button_NavigationMenu'))
			WebUI.waitForElementVisible(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/dropDown_NavigationMenu'), 50)
			selectFromDropDown(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/dropDown_NavigationMenu'),menuItem)
		}

	}

	@Keyword
	public void closeAllTabs() {
		boolean flag=true
		try{
			while(flag)
			{
				if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/button_closeOpenedTabs'), 5))
				{
					WebUI.click(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/button_closeOpenedTabs'))
				}
				else{
					flag==false
					break
				}
			}

		}catch(Exception e)
		{

		}
	}

	@Keyword
	public void closeAllSecondaryTabs() {
		boolean flag=true
		try{
			while(flag)
			{
				if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/button_closeOpenedSecondaryTabs'), 10))
				{
					WebUI.click(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/header_Salesforce/button_closeOpenedSecondaryTabs'))
				}
				else{
					flag==false
					break
				}
			}

		}catch(Exception e)
		{

		}
	}


}
