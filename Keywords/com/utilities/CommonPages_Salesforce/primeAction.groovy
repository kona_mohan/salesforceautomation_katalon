package com.utilities.CommonPages_Salesforce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static org.junit.Assert.*

import java.awt.Robot
import java.awt.event.KeyEvent

import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.testng.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import salesforcePages.loginPage_Salesforce

import com.helper.commonFunctions.JavaScriptFunctions


public class primeAction {

	@Keyword
	public void selectPrimeAction(String primeAction) {
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_chosePrimeAction'), 30)
		WebUI.selectOptionByValue(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_chosePrimeAction'), primeAction, false)
	}

	@Keyword
	public void clickSubmit() {

		WebUI.delay(3)
		JavaScriptFunctions.scrollDownByPixel("500")
		WebUI.waitForElementClickable(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/button_submit'), 40)
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/button_submit'), 40)
		WebUI.click(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/button_submit'))
		WebUI.delay(3)
		Robot robot = new Robot()
		robot.keyPress(KeyEvent.VK_ENTER);
	}

	//nemkonto
	@Keyword
	public void verifySubmitButtonDisabled()
	{
		WebUI.switchToDefaultContent()
		Assert.assertNotNull(WebUI.getAttribute(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/button_submit'), "disabled"))
	}


	//replaceCard
	@Keyword
	public void selectReplaceCardNumber(String cardNumber)
	{
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_replaceCard_cardNumber'), 40)
		WebUI.selectOptionByLabel(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_replaceCard_cardNumber'), cardNumber, false)
	}

	//paymentHoliday
	@Keyword
	public void selectFinancialAccountNumber(String faNumber)
	{
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_paymentHoliday_FAnumber'), 40)
		WebUI.selectOptionByLabel(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_paymentHoliday_FAnumber'), faNumber, false)
	}

	//closeAccount
	@Keyword
	public void selectCloseAccountReason(String reason)
	{
		WebUI.waitForElementPresent(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_CloseAccount_reason'), 40)
		WebUI.selectOptionByLabel(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_CloseAccount_reason'), reason, false)
	}

	@Keyword
	public void verifyAllFieldsMandatory()
	{
		WebUI.switchToDefaultContent()
		WebUI.waitForElementNotVisible(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/span_PleaseWait'), 30)
		WebUI.switchToFrame(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame'), 40)
		List<WebElement> mandatoryFields = WebUiCommonHelper.findWebElements(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/list_mandatoryFields'), 40)
		println mandatoryFields.size()
		WebUI.switchToDefaultContent()
		for(int i=1;i<=mandatoryFields.size()-1;i++)
		{
			WebUI.switchToFrame(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame'), 40)
			mandatoryFields.get(i).findElement(By.xpath(".//span")).getAttribute("style").contains("red")
			WebUI.switchToDefaultContent()
		}

	}


	//create installment
	@Keyword
	public void chooseLoanPeriodInMonths(String months)
	{
		WebUI.switchToDefaultContent()
		WebUI.waitForElementNotVisible(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/span_PleaseWait'), 40)
		if(months.equalsIgnoreCase("6") || months.equalsIgnoreCase("06"))
		{
			TestObject dynamicObject = new TestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/radioButton_createInstallment_loanPeriod').addProperty("xpath", ConditionType.EQUALS, "//input[@value='06']", true)
			WebUI.delay(3)
			WebUI.switchToFrame(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame'), 30)
			WebUI.click(dynamicObject)
			WebUI.switchToDefaultContent()
		}
		else if(months.equalsIgnoreCase("12"))
		{
			TestObject dynamicObject = new TestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/radioButton_createInstallment_loanPeriod').addProperty("xpath", ConditionType.EQUALS, "//input[@value='01']", true)
			WebUI.delay(3)
			WebUI.switchToFrame(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame'), 30)
			WebUI.click(dynamicObject)
			WebUI.switchToDefaultContent()
		}
		else if(months.equalsIgnoreCase("24"))
		{
			println "in else if 2"
			TestObject dynamicObject = new TestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/radioButton_createInstallment_loanPeriod').addProperty("xpath", ConditionType.EQUALS, "//input[@value='02']", true)
			println dynamicObject.xpaths
			WebUI.delay(3)
			WebUI.switchToFrame(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/iFrame'), 30)
			WebUI.click(dynamicObject)
			WebUI.switchToDefaultContent()
		}
		else
		{
			println "incorrect selection"
			Assert.assertTrue(false)
		}
	}

	@Keyword
	public void verifyCustomerNameAutoPopulated()
	{
		WebUI.switchToDefaultContent()
		WebUI.delay(2)
//		WebUI.waitForElementNotVisible(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/span_PleaseWait'), 30)
		Assert.assertNotNull(WebUI.getText(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/text_CustomerName')))
	}

	//sendOutTransactionList
	//	@Keyword
	//	public void selectFinancialAccountSendOutTransactionList(String FAccount)
	//	{
	//		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_sendOutTransactionList_FAnumber'), 30)
	//		WebUI.selectOptionByLabel(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_sendOutTransactionList_FAnumber'), FAccount, false)
	//	}

	//sendOutTransactionList
	@Keyword
	public void downloadTransactionInPDF()
	{
		//		loginPage_Salesforce.setFireFoxPreferences()
		WebUI.waitForElementNotPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/pageLoad_SendOutTransactionList'), 40)
		JavaScriptFunctions.scrollDownByPixel("300")
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/a_SendOutTransaction_downloadPDF'))
		WebUI.delay(5)
		Robot robot = new Robot()
		robot.keyPress(KeyEvent.VK_CONTROL)
		robot.keyPress(KeyEvent.VK_S)
		robot.keyRelease(KeyEvent.VK_CONTROL)
		robot.keyRelease(KeyEvent.VK_S)
		WebUI.delay(3)
		robot.keyPress(KeyEvent.VK_ENTER)
		robot.keyRelease(KeyEvent.VK_ENTER)

	}

	//sendOutTransactionList
	@Keyword
	public void setStartDateAndEndDate(String StartDate, String EndDate)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_sendOutTransacionList_startDate'), 30)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_sendOutTransacionList_startDate'), StartDate)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_sendOutTransacionList_endDate'), 30)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_sendOutTransacionList_endDate'), EndDate)
	}

	//extra card
	@Keyword
	public void selectExistingCard()
	{
		WebUI.waitForPageLoad(5)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/radioButton_extraCard_ExistingCard'), 40)
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/radioButton_extraCard_ExistingCard'), 40)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/radioButton_extraCard_ExistingCard'))
	}

	//extra card
	@Keyword
	public void setSSNNumber(String SSN)
	{
		WebUI.waitForElementNotVisible(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/span_PleaseWait'), 30)
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_extraCard_SSN'), 30)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_extraCard_SSN'), SSN)
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_extraCard_SSN'), Keys.chord(Keys.TAB))
	}

	//extra card
	@Keyword
	public void verifyFirstNameAndLastNameAndEmbossingNameAutoPopulated()
	{
		WebUI.waitForElementNotVisible(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/span_PleaseWait'), 30)
		Assert.assertNotNull(WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_extraCard_firstName'), "value"))
		Assert.assertNotNull(WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_extraCard_lastName'), "value"))
		Assert.assertNotNull(WebUI.getAttribute(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_extraCard_EmbossingName'), "value"))
	}

	//Nemkonto
	@Keyword
	public void selectTransactionType(String transcType)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_Nemkonto_TransactionType'), 30)
		WebUI.selectOptionByValue(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_Nemkonto_TransactionType'), transcType, true)
	}

	//Nemkonto
	@Keyword
	public void setAmount(String transcAmount)
	{
		WebUI.waitForElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_Nemkonto_Amount'), 30)
		WebUI.click(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_Nemkonto_Amount'))
		WebUI.sendKeys(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/input_Nemkonto_Amount'), transcAmount)
	}

	//nemkonto
	@Keyword
	public void verifyErrorMessagePresent()
	{
		WebUI.waitForElementNotPresent(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/span_PleaseWait'), 40)
		Assert.assertNotNull(WebUI.getText(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/errorMessage_Nemkonto')))
	}

	@Keyword
	public void closePrime()
	{
		try{
			WebUI.delay(2)
			WebUI.switchToDefaultContent()
			WebUI.scrollToElement(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/dropDown_chosePrimeAction'), 30)
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/msg_ErrorMessage'), 10))
			{
				WebUI.switchToDefaultContent()
				WebUI.click(findTestObject('OR_Salesforce/com.utilities.CommonPages_Salesforce/primeAction/button_closePrimeAction'))
			}


		}catch(Exception e)
		{

		}


	}


}
