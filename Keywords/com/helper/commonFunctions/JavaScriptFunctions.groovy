package com.helper.commonFunctions

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.Robot
import java.awt.event.KeyEvent

import org.openqa.selenium.By
import org.openqa.selenium.Dimension
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


//import internal.GlobalVariable

public class JavaScriptFunctions {

	public static WebDriver driver

	public static JavascriptExecutor executor


	public static void click(TestObject obj) {
		driver = DriverFactory.getWebDriver()
		executor = ((JavascriptExecutor) driver)
		WebElement clickOnElement = WebUiCommonHelper.findWebElement(obj, 20)
		try {
			if (clickOnElement.isEnabled() && clickOnElement.isDisplayed()) {
				System.out.println("Clicking on element with using java script click");
				executor.executeScript("arguments[0].click();", clickOnElement);
			} else {
				System.out.println("Unable to click on element");
			}
		} catch (Exception e) {
			System.out.println("Element is not found"+ e.getStackTrace());
		}
	}

	public static void clickElement(WebElement clickOnElement) {
		driver = DriverFactory.getWebDriver()
		executor = ((JavascriptExecutor) driver)
		try {
			if (clickOnElement.isEnabled() && clickOnElement.isDisplayed()) {
				System.out.println("Clicking on element with using java script click");
				executor.executeScript("arguments[0].click();", clickOnElement);
			} else {
				System.out.println("Unable to click on element");
			}
		} catch (Exception e) {
			System.out.println("Element is not found"+ e.getStackTrace());
		}
	}


	public static void sendkeys(TestObject obj, String str) {
		driver = DriverFactory.getWebDriver()
		executor = ((JavascriptExecutor) driver)

		WebElement ele = WebUiCommonHelper.findWebElement(obj, 20)

		try {
			if (ele.isEnabled() && ele.isDisplayed()) {
				System.out.println("Sendkeys to element with using java script");
				executor.executeScript("arguments[0].value='"+str+"';", ele)
			} else {
				System.out.println("Unable to sendkeys to element");
			}
		} catch (Exception e) {
			System.out.println("Element is not found"+ e.getStackTrace());
		}
	}

	public static void scrollDownByPixel(String pixelValue) {
		driver = DriverFactory.getWebDriver()
		executor = ((JavascriptExecutor) driver)
		executor.executeScript("window.scrollBy(0,"+pixelValue+")")
	}

	public static void scrollUpByPixel(String pixelValue) {
		driver = DriverFactory.getWebDriver()
		executor = ((JavascriptExecutor) driver)
		executor.executeScript("window.scrollBy("+pixelValue+",0)")
	}

	public static void scrollToElement(TestObject obj) {

		WebElement ele = WebUiCommonHelper.findWebElement(obj, 20)
		driver = DriverFactory.getWebDriver()
		executor = ((JavascriptExecutor) driver)
		executor.executeScript("arguments[0].scrollIntoView(false);", ele)
	}

	//	public static String getText(TestObject obj) {
	//		WebElement ele = WebUiCommonHelper.findWebElement(obj, 20)
	//		driver = DriverFactory.getWebDriver()
	//		executor = ((JavascriptExecutor) driver)
	//		String text = (String) executor.executeScript("return arguments[0].text;", ele)
	//		return text
	//	}


	//	public static void setZoomLevel()
	//	{
	////		if(GlobalVariable.Jenkins)
	//		{
	//			WebUI.delay(10)
	//			Dimension dimension = new Dimension(1032, 776);
	//			//1032, 776
	//			//1044, 788
	//			DriverFactory.getWebDriver().manage().window().setSize(dimension)
	//			//			Robot robot = new Robot();
	//			//
	//			//			robot.keyPress(KeyEvent.VK_CONTROL);
	//			//			robot.keyPress(KeyEvent.VK_MINUS);
	//			//			robot.keyRelease(KeyEvent.VK_CONTROL);
	//			//			robot.keyRelease(KeyEvent.VK_MINUS);
	//			//
	//			//			robot.keyPress(KeyEvent.VK_CONTROL);
	//			//			robot.keyPress(KeyEvent.VK_MINUS);
	//			//			robot.keyRelease(KeyEvent.VK_CONTROL);
	//			//			robot.keyRelease(KeyEvent.VK_MINUS);
	//			//
	//			//			robot.keyPress(KeyEvent.VK_CONTROL);
	//			//			robot.keyPress(KeyEvent.VK_MINUS);
	//			//			robot.keyRelease(KeyEvent.VK_CONTROL);
	//			//			robot.keyRelease(KeyEvent.VK_MINUS);
	//			//
	//			//			robot.keyPress(KeyEvent.VK_CONTROL);
	//			//			robot.keyPress(KeyEvent.VK_MINUS);
	//			//			robot.keyRelease(KeyEvent.VK_CONTROL);
	//			//			robot.keyRelease(KeyEvent.VK_MINUS);
	//			//
	//			//			robot.keyPress(KeyEvent.VK_CONTROL);
	//			//			robot.keyPress(KeyEvent.VK_MINUS);
	//			//			robot.keyRelease(KeyEvent.VK_CONTROL);
	//			//			robot.keyRelease(KeyEvent.VK_MINUS);
	//			//
	//			//			robot.keyPress(KeyEvent.VK_CONTROL);
	//			//			robot.keyPress(KeyEvent.VK_MINUS);
	//			//			robot.keyRelease(KeyEvent.VK_CONTROL);
	//			//			robot.keyRelease(KeyEvent.VK_MINUS);
	////						WebUI.takeScreenshot("C:/Users/MohanKoDownloads/Entercard1/Screen Shots/"+GlobalVariable.testCaseName+".png")
	//		}
	//		//		WebElement ele = DriverFactory.getWebDriver().findElement(By.xpath("//*[contains(@class,'uiInput')]//input[@placeholder]"))
	//		//		ele.sendKeys(Keys.CONTROL,Keys.chord("-"))
	//		//			ele.sendKeys(Keys.CONTROL,Keys.chord("-"))
	//		//				ele.sendKeys(Keys.CONTROL,Keys.chord("-"))
	//	}
}
