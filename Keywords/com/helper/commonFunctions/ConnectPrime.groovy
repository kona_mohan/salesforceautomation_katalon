package com.helper.commonFunctions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.sun.java.util.jar.pack.Package.Class

import internal.GlobalVariable

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.testng.Assert

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword


public class ConnectPrime {

	public static Connection conn = null;
	public static Statement stmt = null;
	public static ResultSet resultSet = null;
	public static void connectToDataBase() {

		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection(GlobalVariable.databaseURL, GlobalVariable.databaseUser, GlobalVariable.databasePassword);
		stmt = conn.createStatement();
	}

	public static void disConnectToDataBase() {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (Exception e) {
			}
		}

		if (stmt != null) {
			try {
				stmt.close();
			} catch (Exception e) {
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}



	@Keyword
	public static void  executeStatementAndVerify(String sqlQuery, String valueToBeVerified) {
		connectToDataBase()
		resultSet = stmt.executeQuery(sqlQuery);
		resultSet.next()
		System.out.println(resultSet .getString(1));
		Assert.assertEquals(resultSet.getString(1).trim(), valueToBeVerified)
		disConnectToDataBase()
	}
	
	@Keyword
	public static String executeStatement(String sqlQuery) {
		connectToDataBase()
		resultSet = stmt.executeQuery(sqlQuery);
		resultSet.next()
		System.out.println(resultSet .getString(1));
		return resultSet .getString(1)
		disConnectToDataBase()
		
	}
}








