package com.helper.commonFunctions

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI



public class ActionClassFunctions {

	public static WebDriver driver

	public static Actions action


	public static scrollToElement(TestObject obj) {
		driver = DriverFactory.getWebDriver()
		action = new Actions(driver)
		println "Scrolling to element"
		WebElement elementToBeClicked = WebUiCommonHelper.findWebElement(obj, 50)
		action.moveToElement(elementToBeClicked).build().perform()
	}

	public static click(TestObject obj) {
		driver = DriverFactory.getWebDriver()
		action = new Actions(driver)
		WebElement elementToBeClicked = WebUiCommonHelper.findWebElement(obj, 50)
		action.moveToElement(elementToBeClicked).click().build().perform()
	}
}
